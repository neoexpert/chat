window.onerror = Fehlerbehandlung;
 
    function Fehlerbehandlung (Nachricht, Datei, Zeile) {
      Fehler = "Fehlermeldung:\n" + Nachricht + "\n" + Datei + "\n" + Zeile;
      zeigeFehler();
      return true;
    }
 
    function zeigeFehler () {
      alert(Fehler);
    }
document.addEventListener('DOMContentLoaded', function () {
  if (!Notification) {
    return;
  }

  if (Notification.permission !== "granted")
    Notification.requestPermission();
});
//navigator.serviceWorker.register('sw.js');

function showNotification() {
  Notification.requestPermission(function(result) {
    if (result === 'granted') {
      navigator.serviceWorker.ready.then(function(registration) {
        registration.showNotification('Vibration Sample', {
          body: 'Buzz! Buzz!',
          icon: '../images/touch/chrome-touch-icon-192x192.png',
          vibrate: [200, 100, 200, 100, 200, 100, 200],
          tag: 'vibration-sample'
        });
      });
    }
  });
}
function onClose() {
$("#chat").html("disconnected");
	connect();
}
function onOpen(){
 	window.onfocus = function() {connect(); };
$("#chat").html("");
}
function js(j){
return JSON.stringify(j);
}
var lang = navigator.language || navigator.userLanguage; 
function sendMsg(event,me) {
    if(event.keyCode == 13) {
	var msg=me.value;
	if(me.value.startsWith("/nick "))
	{
		var name=msg.substring(msg.indexOf(" ")+1,msg.length);
		var m={type:"setname",name:name}
  		c.send(js(m));
		return;
	}
	var m={type:"usermsg",message:msg,message_type:"text"}
	if(lang)
		m["lang"]=lang;
	if(country)
		m["country"]=country;
	if(ref){
		m["refer"]=ref;
		ref=undefined;
	}
  	c.send(js(m));

    }
}
function private_chat(id){
}
function hide(_id){
	var hm={type:"toggle_message",id:_id,visible:false,lang:lang};
	c.send(js(hm));
}
var ref;
function refer(id,name){
	ref=id;
var txtarea = document.getElementById("msginput");
    // obtain the index of the first selected character
    var start = txtarea.selectionStart;
    // obtain the index of the last selected character
    var finish = txtarea.selectionEnd;
    //obtain all Text
    var allText = txtarea.value;

    // obtain the selected text
    var sel = allText.substring(start, finish);
    //append te text;
    var newText=allText.substring(0, start)+"@"+name+allText.substring(finish, allText.length);

    console.log(newText);

    txtarea.value=newText;
}
function processMsg(msg,next){
switch(msg.message_type){
	case "text":
		view.addMessage(msg,next);
	break;
	case "rocket":
		launch();
	break;
}
}
var cl={};
var ls;
cl.nextMessage=function(){
	c.send(js({type:"next"}));
}
function connect(){
	if(c)
		if(c.readyState==1)return;
	c= new WebSocket("ws://" + location.host);
	c.onopen = onOpen;
	c.onclose = onClose;
	c.onerror = onError;
	c.onmessage = onMessage;
}
var c;
var room="scdown";
cl.moderator=false;
$(function(){
 room=window.location.pathname.substring(1);
	ls=localStorage;
	msginput=$("#msginput");
	connect();
	var canvas = document.getElementById('canvas');
       var context = canvas.getContext('2d');

    // resize the canvas to fill browser window dynamically
    window.addEventListener('resize', resizeCanvas, false);

    function resizeCanvas() {
            canvas.width = window.innerWidth;
            canvas.height = window.innerHeight;

    }
    resizeCanvas();
var pressTimer;
(function($) {
    $.fn.longClick = function(callback, timeout) {
        var timer;
        timeout = timeout || 500;
        $(this).mousedown(function() {
            timer = setTimeout(function() { callback(); }, timeout);
            return false;
        });
        $(document).mouseup(function() {
            clearTimeout(timer);
            return false;
        });
    };

})(jQuery);
//$(window).longClick(function(){
//alert();
//});
});
function onError(error) {
$("body").append(error);
  console.log('WebSocket Error ' + error);
};

var users=0;
var msginput;

function onMessage(e) {
	var msg=JSON.parse(e.data);	
	//$("body").append(e.data+"<br>");
	switch(msg.type){
		case "error":
			alert(msg.err);
		break;
		case "yourID":
		cl.myID=localStorage.getItem("myID");
		var pw=localStorage.getItem("pw");
		var l={type:"myID",myID:cl.myID,pw:pw,version:"web"};
		var mycolor=ls.getItem("color");
		if(room=="chat.html")
			room="scdown";
		if(room=="")
			room="default";
		if(room)
		l["room"]=room;
		if(!mycolor)
		{
			l["color"]=color;
			ls.setItem("color", color);
		}
  		c.send(js(l));
		break;
		case "yourNewID":
		ls.setItem("myID", msg.id);
		ls.setItem("pw", msg.pw);
		break;
		case "youAreOnline":
  		c.send(js({type:"messagelist",room:room}));
		break;
		case "youaremoderator":
		cl.moderator=true;
		break;
		case "usermsg":
			processMsg(msg);
		break;
		case "nextmsg":
			processMsg(msg,true);
		break;
		case "seen":
		$("#"+msg.id).find("msgtext").append("<div class='seen'>✓<div>");
		break;
		case "received":
		msginput.val("");
		break;
		case "userison":
			if(msg.room==room)
			users++;	
		msginput.attr("placeholder", "online: "+users);
		break;
		case "userisoff":
			if(msg.room==room)
			users--;
		break;
		case "toggle_message":
		case "delete":
		$("#"+msg.id).hide();
		break;
		default:
		alert(e.data);
		break;
	}
};
