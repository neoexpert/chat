module.exports.onlog = onlog;
Tail = require('tail').Tail;
function onlog(callback){
	var auth;
	var messages; 
	try{
		auth = new Tail("/var/log/auth.log");
		messages = new Tail("/var/log/messages");
	}
	catch(e){
		console.error(e);
		return;
	}

	auth.on("line", callback);
	messages.on("line", callback);

	auth.on("error", function(error) {
	  console.log('ERROR: ', error);
	});
}

