module.exports.start = start;
module.exports.sendToAdmin = sendToAdmin;
const wsServer = require('ws').Server;
const bot = require('./bot');
const mongodb = require('mongodb');
const authlog = require('./authlog');
//const GoogleAuth = require('google-auth-library');
const https = require('https');
//var auth = new GoogleAuth;
//const GCLIENT_ID="665566840894-f8j7n9dhtgp8hqvimoemhlj429pv3bh5.apps.googleusercontent.com";
//var gauthclient = new auth.OAuth2(GCLIENT_ID, '', '');
var clients = [];
var wss;
var db,users,factsdb,nodes;
var facts={};
var lastRegisterTS=0;
var maxID = 0;
var rooms={};
function logerr(msg){
	console.error(new Date().toISOString()+" ERROR: "+msg);
}
function log(msg){
	console.log(new Date().toISOString()+": "+msg);
}
class User {
	constructor(con, id, admin, name, ts) {
		this.con = new Set();
		this.con.add(con);
		this.id = parseInt(id);
		this.admin = admin;
		this.name = name;
		this.ts = ts;
	}
}
function start(webServer,_db,_users,_factsdb,_nodes){
	db=_db;
	users=_users;
	factsdb=_factsdb;
	nodes=_nodes;
	db.findOne({
		id: "maxID"
	},function(err, doc) {
			maxID = doc.value;
			log("maxID: "+maxID);
		});
	
	wss = new wsServer({
		server: webServer
	});
	wss.on('connection', onConnection);
}
/*
function glogin(msg,user,con){
	gauthclient.verifyIdToken(
		msg.token,
		GCLIENT_ID,
	function(e, login) {
		if(e){
			logerr(e);
			return;
		}

		var payload = login.getPayload();
		var userid = payload['sub'];
		log("google login: "+userid);
		users.findOne({
			type: "user",
			gid: userid
		}, function(e, u) {
		if (u == null)
		{
			users.findOneAndUpdate({type: "user",id:user.id}, {$set: {"gid": userid,guser:payload}},{}, function(err,r) {});
		}
		else{
			if(u.id!=user.id){
				var json2 = JSON.stringify({
					type: 'YourNewID',
					id: u.id,
					pw: u.pw
				});
				log("user registered: "+json2);
				con.send(json2,sendcallback);
			}
			users.findOneAndUpdate({type: "user",id:u.id}, {$set: {guser:payload}},{}, function(err,r) {});
		}
		});
		// If request specified a G Suite domain:
		//var domain = payload['hd'];
	});
}
*/
function js(j){
	return JSON.stringify(j);
}
function sendcallback(err){
	if(err){
		logerr("send error:");
		logerr(err);
	}
}
facts.update=function(s,fact,inc){
	factsdb.update(s,{$set:fact,$inc:{count:inc}},{upsert:true});
}
facts.find=function(f,callback){
	var c=factsdb.find(f);
	c.sort({count:-1});
	c.limit(10);
	c.toArray(
		function(err, msgs) {
			if(err)return;
			if(msgs.length<1)return;
			callback(msgs);
		});
}

facts.remove=function(f,callback){
	factsdb.deleteOne(f);	
}

facts.findOne=function(_f,_callback){
	let f=_f;
	let callback=_callback;
	var c=factsdb.find(f);
	c.sort({count:-1});
	c.limit(1);
	c.toArray(
		function(err, msgs) {
			if(err){
				logerr(err);
				return
			};
			if(msgs.length>0){
				callback(msgs[0]);
			}
			else
				callback();

		});
}

authlog.onlog(function(data){
	var dbmsg = {
		type: 'message',
		name: "S",
		message: data,
		message_type: "authlog",
		from: 0
	};
	dbmsg["ts"] = Date.now();
		db.insert(dbmsg,
			function(err, record){
				dbmsg["type"] = "usermsg";
				sendToAdmin(dbmsg);
			});

});

/*
function verifyRecaptcha(key, callback) {
		var SECRET="6LcAYyYUAAAAAEZ_FOskrw3TTxN10FaZg9oDcB3E";
    https.get("https://www.google.com/recaptcha/api/siteverify?secret=" + SECRET + "&response=" + key, function(res) {
        var data = "";
        res.on('data', function (chunk) {
                        data += chunk.toString();
        });
        res.on('end', function() {
            try {
                var parsedData = JSON.parse(data);
                callback(parsedData.success);
            } catch (e) {
                callback(false);
            }
        });
    });
}
*/
function sendToAdmin(msg){
	var json = JSON.stringify(msg);
	for (var cid in clients) {
		if (clients[cid] == undefined) continue;
		if (true === clients[cid].admin)

			sendToUser(clients[cid],json);
	}
}

/*
function sendToRoom(room,msg){
		var json = JSON.stringify(msg);
			for (var cid in clients) {
				if (clients[cid] == undefined) continue;
					//if (room === clients[cid].room)
					sendToUser(clients[cid],json,room);
			}
}*/

function sendToUser(user,msg,room){
	if(user)
		for (let c of user.con) {
			if(!room||c.room==room){
				try{
					c.send(msg);
				}
				catch(e){
					logerr(user.id+" was not connected. Connections: "+user.con.size);
					logerr("c:"+c);
					disconnect(user,c);
				}
			}
		}
}

function filterMessage(msg){
				var dbmsg = {};
				dbmsg["type"] = 'usermsg';
				dbmsg["_id"] = msg._id;
				dbmsg["name"] = msg.name;
				dbmsg["message"] = msg.message;
				dbmsg["message_type"] = msg.message_type;
				dbmsg["from"] = msg.from;
				dbmsg["room"] = msg.room;
				dbmsg["ts"] = msg.ts;
				if (msg.to)
								dbmsg["to"] = msg.to;
				if (msg.refer)
								dbmsg["refer"] = msg.refer;
				if (msg.lang)
								dbmsg["lang"] = msg.lang;
				if (msg.country)
								dbmsg["country"] = msg.country;
				if (msg.hasOwnProperty("visible"))
								dbmsg["visible"] = msg.visible;
				if (msg.toggled_from)
								dbmsg["toggled_from"] = msg.toggled_from;
				if (msg.color)
								dbmsg["color"] = msg.color;
				if (msg.ts)
								dbmsg["ts"] = msg.ts;
				if (msg.admin)
								dbmsg["admin"] = msg.admin;
				if (msg.moderator)
								dbmsg["moderator"] = msg.moderator;
				if (msg.baned)
								dbmsg["baned"] = msg.baned;
				if (msg.seen)
								dbmsg["seen"] = msg.seen;
				return dbmsg;
}

function processMessage(msg, user,con) {
	if(msg.message_type=="text")
		users.update({type: "user",id:user.id}, {$inc: {"messages": 1}},{}, function() {});
	if(msg.message_type=="rocket")
		users.findOneAndUpdate({type: "user",id:user.id}, {$inc: {"rockets": 1}},{}, 
		function(err,r) {
			if(err)
				return;
			if(!r.value)return;
			var r=parseInt(r.value.rockets);
			var i=r;
			if(i>=100)
			while(i>=10){
				i/=10;
				if(i==1||i==2||i==5)
				{
					sm(user.name+": "+r+" rockets",user.id,user.room);
					break;
				}
			}
		});
	if (con.room != undefined) {
		//if(msg.message_type=="text")
			//log(msg.name+" ("+con.room+"): "+msg.message);
		msg["type"] = "usermsg";
		var json = js(msg);
		if(msg.to)
		{
			sendToUser(user,json,con.room);
			if(msg.baned)return;
			var to=parseInt(msg.to);
			userExists(to,function(){
				sendToUser(clients[to],json,con.room);
			});
		}
		else
			for (var cid in clients) {
				if (clients[cid] == undefined) continue;
				//if (user.room === clients[cid].room)
				if(!msg.baned||clients[cid].admin||cid==user.id)
					sendToUser(clients[cid],json,con.room);

			}
			return;
	}
}

function userExists(id, callback) {
	users.findOne({id: id},
		function(e, u) {
			if (u != null)
				callback();
		});
}

var lastRegisterTS=0;;

function checkUser(con, msg, callback) {
	var id = parseInt(msg.myID);
	var pw = msg.pw;
	if (id == 0 || isNaN(id)) {
		var cts=Date.now();
		if(cts-lastRegisterTS<1000){
				callback("wait",null);
				return;
		}
		lastRegisterTS=Date.now();
		id=saveUser(id, pw,con);
		var user = processUser(con, id, msg);
		callback(null,user);
	} 
	else {
		users.findOne({
			type: "user",
			id: id
		}, function(e, u) {
			if(e){
				logerr(e);
				return;
			}
			if (u == null){
				saveUser(id, pw,con);
				callback("wronglogin",null);
			}
			else if (u.pw === pw) {
				var user = processUser(con, id, msg,u);
				callback(null,user);
			} 
			else
			{
				saveUser(id, pw,con);
				callback("wronglogin",null);
			}
		});
	}
}

var conid=0;
function processUser(con, id, msg,u) {
	var user = null;
	if((typeof msg.room)=="string")
	{
		if(!rooms[msg.room])
			rooms[msg.room]={};
					rooms[msg.room][conid]=con;
					con.room=msg.room;
					con.conid=conid;
					conid++;
				}
				if (clients[id] === undefined) {
								user = new User(con, 0, false, "", Date.now() / 1000);
								clients[id] = user;
				} else {
								clients[id].con.add(con);
								user = clients[id];
				}


				log("New Login: id: " + id);

				clients[id].info = msg.info;
				clients[id].id = id;
				clients[id].name = msg.name;
				clients[id].version = msg.version;
				clients[id].room = msg.room;
				if(u){
					db.count({from:u.id,visible:false,toggled_from:{$ne:u.id}},
						function(err,count){
							if(err)return;	
								users.update({id:id}, {$set: {"hiddenmsgs":count,online:true}}, {}, function() {});

																});
						var toset={"online_ts": Date.now(),msgratio:u.hiddenmsgs/u.messages}
						if(msg.color)
							toset["color"]=msg.color;
						users.update({id:id}, {$set: toset}, {}, function() {});
						user.moderator=u.moderator;
						user.ban=u.ban;
						user.color=u.color;
						if (u.moderator) {
							var json = JSON.stringify({
								type: 'youaremoderator',
								id: u.id
							});
							con.send(json,sendcallback);
						}
						if (u.ban>Date.now()) {
							var json = JSON.stringify({
								type: 'youarebanned',
								id: u.id
							});
							con.send(json,sendcallback);
						}
						user.admin = u.admin;
						if (u.admin) {
							var json = JSON.stringify({
								type: 'youareadmin',
								id: u.id
							});
							con.send(json,sendcallback);
						}
						user.name=u.name;
	}
	var yao=js(
		{
			type: 'youAreOnline',
			doc:u,
			usersonline:Object.keys(rooms[con.room]).length
		});
	con.send(yao,sendcallback);
	var json = js({
		type: 'userison',
		name: msg.name,
		id: id,
		admin: user.admin,
		room: user.room,
		version: user.version
	});
	for (var cid in clients) {
		if (cid == id) continue;
		if (clients[cid] == undefined) continue;
		var json2 = JSON.stringify({
			type: 'userison',
			name: clients[cid].name,
			id: cid,
			admin: clients[cid].admin,
			room: clients[cid].room,
			version: clients[cid].version,
			usersonline:Object.keys(rooms[con.room]).length
		});
		if(user.version<=180)
			con.send(json2,sendcallback);
		sendToUser(clients[cid],json,con.room);
	}

	return user;
}

function disconnect(user,con){
	if((typeof con.room)=="string"&&con.conid)
	{
		if(rooms[con.room])
			delete rooms[con.room][con.conid];
	}
	user.con.delete(con);
	log("connection closed. remainig connections: " + user.con.size);
	if (user.con.size == 0) {
		users.update({type:"user",id:user.id}, {$set: {online:false}}, {}, function() {});
		
		clients[user.id] = undefined;
		log("user " + user.id + " disconnected");
		var json = js({
			type: 'userisoff',
			id: user.id,
			room: user.room
		});
		for (var cid in clients) {
			if (clients[cid] === undefined) continue;
			if (cid == user.id) continue;
			sendToUser(clients[cid],json);
		}
	}
	if(con.cursor){
		con.cursor.close();
		log("cursor closed");
	}
	if(con.ucursor){
		con.ucursor.close();
		log("ucursor closed");
	}
	if(con.fcursor){
		con.fcursor.close();
		log("fcursor closed");
	}
}

function sendToRoom(room,msg){
	if(!rooms[room])return;
	var json = JSON.stringify(msg);
	var mroom=rooms[room];
	for (var r in mroom) {
		if (mroom.hasOwnProperty(r))
			try {
				mroom[r].send(json,sendcallback);
			} 
			catch (e) {
				logerr("mroom is broken:"+r);
			}
					
	}
}
function processBot(umsg,user){
	return function(){
		var msg=umsg.message;
		if((typeof msg)!="string")
			return;
		bot.process(facts,msg,user,function(a){
			sm(a,user.id,user.room);
		});
	if(msg.refer==="0")
	{
		if(msg.length<100)
		{
			msg=msg.replace(/<span style=\"color:#bbd43d;\">s/g, msg.name);
			msg=msg.replace(/ich/g, 'Du');
			sm(msg,user.id,user.room);
		}
		return;
	}
}
}
function sm(msg,refer,room,type,from,name){
	if(!type)
		type="text";
	var dbmsg = {
		type: 'message',
		message: msg,
		message_type: type,
		room:room
	};
	if(from)
		dbmsg.from=from;
	else
		dbmsg.from=0;
	if(name)
		dbmsg.name=name;
	else
		dbmsg.name="S";;
	dbmsg["ts"] = Date.now();
	if(refer)
		dbmsg["refer"] = refer;
		db.insert(dbmsg,
			function(err, record){
				dbmsg["type"] = "usermsg";
				for (var cid in clients) {
					if (clients[cid] == undefined) 
						continue;
					if (room === clients[cid].room)
						sendToUser(clients[cid],js(dbmsg));
				}
			});
}

function guid() {
	function s4() {
		return Math.floor((1 + Math.random()) * 0x10000)
			.toString(16)
			.substring(1);
	}
	return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
		s4() + '-' + s4() + s4() + s4();
}
function incrementMaxId(){
	maxID++;
	db.update({id: "maxID"}, 
	{
$set: {"value": maxID}
}, {}, function() {});
return maxID;
}
function saveUser(id, pw,con) {
	id=incrementMaxId();
	var pw = guid();
	var json2 = js({
		type: 'YourNewID',
		id: id,
		pw: pw
	});
	con.send(json2,sendcallback);
	users.insert({
		type: "user",
		id: id,
		pw: pw,
		created: Date.now()
	});
	return id;
}


function onConnection(ws) {
	var user = null;
	ws.on('message', onMessage);
	ws.on('error', onError);
	ws.on('close', onClose);

	var json = js({
		type: 'yourID'
	});
	ws.send(json,sendcallback);
	var mc=0;
	var lastMsgTS=0;
	function onError(message) {
		logerr(message);
	}
	function onMessage(message) {
		var dt=Date.now() - lastMsgTS;
		if (dt > 10000)
			mc = 0;
		if (mc == 0)
			lastMsgTS = Date.now();
		if (mc > 100)
		{
				log("spamfilter ("+user.name+" ID: "+user.id+")");
				var json = js({
					type:"error",
					err: "wait "+Math.ceil((10000-dt)/1000)+" s"
				});
				ws.send(json,sendcallback);
				if(user==null)
					ws.close();
				lastMsgTS += 1000;
					if(user.admin!=true)
					return;
		}
/*if (message.type === 'binary') {
	try {
	var msg = BSON.deserialize(message.binaryData);
	var to = parseInt(msg.to);
	console.log("binary to: " + to);
	msg.from = user.id;
	var data = BSON.serialize(msg, false, true, false);
	var it = clients[to].con.values();
	var con = it.next();
//console.log(con)
con.value.sendBytes(data);
} catch (e) {
console.log(e);
}
}*/
//if (message.type === 'utf8') { // accept only text
	//console.log(message);
	if(message.length>500)
		mc+=5;
	try {
		var msg = JSON.parse(message);
	} catch (e) {
		logerr(e);
		ws.close();
		return;
	}
if(msg.type=="myID") {
		checkUser(ws, msg, 
			function(err,newUser) {
				if(err){
					var json = js({
						type:"error",
						err: err
					});
					ws.send(json,sendcallback);
					ws.close();
					return;
				}
				if(newUser === null)
					ws.close();
				else
					user = newUser;
			});
	return;
}
if (user === null) return;
switch(msg.type){
		/*
	case "glogin":
		glogin(msg,user,ws);
	return;
	case "recaptcha":
		verifyRecaptcha(msg.key,function(result){
			if(result){
				users.findOneAndUpdate({type: "user",id:user.id}, {$set: {"not_a_bot": +new Date()}},{}, function(err,r) {});
				ws.send(js({type:"not_a_bot"}),sendcallback);
			}
		});
	return;
	*/
	case "updateMessage":
		var filter={};
		filter.type="message";
		var _id;
		try{
			_id=new mongodb.ObjectID(msg._id);
		}
		catch(e)
		{
			logerr(e);
			return;
		}
		filter._id=_id;
		db.findOne(filter, function(e, m) {
			if (m){
				if(m.from==user.id){
					db.update(filter, {$set: {updatedMessage:msg.message}},{}, function(err,n){});
					var uMsg={type:"updateMessage",_id:m._id,message:msg.message};
						sendToRoom(ws.room,uMsg);
				}
			}
		});
	return;
	case "usermsg":
		mc++;
		var dbmsg = {};
		dbmsg["type"] = 'message';
		dbmsg["_id"] = msg._id;
		if(user.name)
						dbmsg["name"] = user.name;
		else
						dbmsg["name"] = msg.name;
		dbmsg["message"] = msg.message;
		dbmsg["message_type"] = msg.message_type;
		if(msg.to)
						dbmsg["to"] = parseInt(msg.to);
		dbmsg["from"] = user.id;
		dbmsg["room"] = ws.room;
		dbmsg["ts"] = Date.now();
		if (msg.refer)
						dbmsg["refer"] = msg.refer;
		if (msg.lang)
						dbmsg["lang"] = msg.lang;
		if (msg.country)
						dbmsg["country"] = msg.country;
		if (user.moderator)
						dbmsg["moderator"] = true;
		if (user.admin)
						dbmsg["admin"] = true;
		if (user.color)
						dbmsg["color"] = user.color;
		var baned=user.ban>Date.now();
		if(baned)
						dbmsg["baned"]=baned;
		db.insert(dbmsg,function(err, record){
										ws.send(js({type:"received",message_type:msg.message_type,to:msg.to}),sendcallback);
										if(msg.message_type=="text")
										mc+=4;
										processMessage(dbmsg, user,ws);
										if(!dbmsg.to&&!msg.baned)
										setTimeout(processBot(dbmsg,user), 1);
										});
		return;
	case "userlist":
		for (var cid in clients) {
			if (cid == user.id) continue;
			if (clients[cid] == undefined) continue;
			//console.log(JSON.stringify(client));
	
			var json = js({
				type: 'userison',
				name: clients[cid].name,
				id: cid,
				admin: clients[cid].admin,
				room: clients[cid].room,
				version: clients[cid].version
			});
	//sendToUser(user,json);
			ws.send(json,sendcallback);
		}
	return;
	case "setChessState":
		if(!msg.player)return;
		var player=parseInt(msg.player);
		var bid;
		if(player<user.id)
			bid=player+"_"+user.id;
		else
			bid=user.id+"_"+player;
		var filter={};
		filter["type"]="chess";
		filter["board"]=bid;
		db.findOne(filter, function(e, u) {
			var allowed=false;
			var wt=true;
			if (u){
				if(u.wt){
						if(user.id<=player)
								allowed=true;
				}
				else{
						if(user.id>=player)
								allowed=true;
				}
				wt=u.wt;
			}
			else allowed=user.id<=player;
			if(allowed)	
				db.findOneAndUpdate(
    			{type: "chess",board:bid},
    			{$set: {map: msg.map,wt:!wt}},
    			{upsert: true, safe: false,returnOriginal:false},
    			function(err,data){
    			    if (err){
    			        logerr(err);
    			    }else{
								var board=data.value;
								board["player"]=user.id;
								var json = js(board);
								sendToUser(clients[player],json);
    			    }
    			});
				logerr("not allowed to setChessState");
		});
	break;
	case "getChessState":
		if(!msg.player)return;
		var player=parseInt(msg.player);
		var bid;
		if(player<user.id)
			bid=player+"_"+user.id;
		else
			bid=user.id+"_"+player;
		var filter={};
		filter["type"]="chess";
		filter["board"]=bid;
		db.findOne(filter, function(e, u) {
			if (u){
				u["player"]=player;
				var json = js(u);
				ws.send(json,sendcallback);
			}
		});
	break;
	case "updateme":
		var data=msg.data;
		if(data.id)return;
		users.findOneAndUpdate({type: "user",id:user.id}, {$set: data},{returnOriginal:false},
			function(err,doc) {
				if(err){
					var json = js({type:"error",err:"nameexists"});
												//sendToUser(user,json);
							ws.send(json,sendcallback);
				}
				else{
					var data={type: 'userupdate',doc:doc.value};
					sendToRoom(ws.room,data);
				}
			});
	return;
	case "findOne":
		if(!msg.filter)return;
		var filter=msg.filter;
		users.findOne(filter, function(e, u) {
			if (u){
				var r={};
				u["pw"]=undefined;
				r["type"]=msg.return_type;
				r["doc"]=u;
				var json = js(r);
				ws.send(json,sendcallback);
			}
		});
	return;
	case "find":
		if(!msg.filter)return;
		var filter=msg.filter;
		filter["to"]={$exists:false};
		var c;
		switch(msg.collection)
		{
			case "users":
				c=users.find(filter);
				if(msg.sort)
					c.sort(msg.sort);
				ws.ucursor=c;
			break;
			case "facts":
				c=factsdb.find(filter);
				if(msg.sort)
					c.sort(msg.sort);
				ws.fcursor=c;
			break;
			default:
			filter["room"]=ws.room;
				c=db.find(filter);
				c.sort({ts:-1});
				ws.cursor=c;
			break;
		}
		c.cb=msg.cb;
	//return;
	case "next":
		var c;
		var return_type="nextmsg";
		switch(msg.collection){
			case "users":
				c=ws.ucursor;
				return_type="nextuser";
			break;
			case "facts":
				c=ws.fcursor;
				return_type="nextfact";
			break;
			default:
				c=ws.cursor;
			break;

		}
		mc--;
		if(!c){log("cursor is null");return;}
		c.nextObject(function(err, nextmsg) {
			if(err){
				logerr("cursor error");
				ws.cursor=null;
				logerr(err);
				var json = js({type:"refresh_cursor",reason:"cursor is null"});
				ws.send(json,sendcallback);
				return;
			}
			if(!nextmsg)return;
			if(msg.ts)
			if(nextmsg.ts>msg.ts){
				var json = js({type:"refresh_cursor",reason:"ts",ts:nextmsg.ts,msgts:msg.ts});
				ws.send(json,sendcallback);
				logerr("nextmsg.ts>msg.ts");
				return;
			}
			//var dbmsg = filterMessage(nextmsg);;
			var dbmsg = nextmsg
			dbmsg["pw"]=undefined;
			dbmsg["type"]=return_type;
			var json = js(dbmsg);
			ws.send(json,sendcallback);
		});
	return;
	case "clearCursor":
		switch(msg.collection){
			case "users":
				ws.ucursor=null;
			break;
			case "facts":
				ws.fcursor=null;
			break;
			default:
				ws.cursor=null;
			break;
		}
	return;
	case "findsome":
		if(!msg.filter)return;
		var filter = msg.filter;
		filter["type"] = "message";
		ws.cursor=
			db.find(filter);
		var c = db.find(filter);
		c.sort({ts: -1});
		c.limit(20);
		c.toArray(function(err, msgs) {
			if(err){
				logerr(err);
				return;
			}
			for (var i = msgs.length - 1; i >= 0; i--) {
				var msg = msgs[i];
				msg["type"]="foundsome";
				var json = js(msg);
				ws.send(json,sendcallback);
			}
		});
	return;
	case "messagelist":
		var filter = {$or:[{to:{$exists:false}},{to:user.id}]};
		filter["type"] = "message";
		filter["room"] = msg.room;
		if (msg.lang)
			filter["lang"] = msg.lang;
		if (msg.message_type)
			filter["message_type"] = msg.message_type;
		else
			filter["message_type"] = {$in:["text","name_change"]};
		log(js(filter["message_type"]));
		if(user.admin|| user.moderator)
		{
			if(msg.hasOwnProperty("visible"))
				filter["visible"] = msg.visible;
		}
		else
		{
			filter["visible"]={$ne:false};
			filter["baned"]={$ne:true};
		}

		ws.cursor=db.find(filter);
		ws.cursor.sort({ts: -1});
		ws.cursor.skip(20);
		var c = db.find(filter);
		c.sort({ts: -1});
		c.limit(20);
		c.toArray(
			function(err, msgs) {
				if(err){
					logerr(err);
					return;
				}
				for (var i = msgs.length - 1; i >= 0; i--) {
					var msg = msgs[i];
					var dbmsg = filterMessage(msg);;
					dbmsg["history"]=true;
					var json = JSON.stringify(dbmsg);

					if(msg.to){
						if(msg.to==user.id)
							ws.send(json,sendcallback);
										//sendToUser(user,json);
					}
					else
						ws.send(json,sendcallback);
												//sendToUser(user,json);

			}
		});

	return;
	case "getunseen":
		var filter = {};
		filter["type"] = "message";
		filter["to"]=user.id;
		filter["seen"]={$exists:false};
		var c = db.find(filter);
		c.sort({ts: -1});
		c.limit(20);
		c.toArray(function(err, msgs) {
							for (var i = msgs.length - 1; i >= 0; i--) {
								var msg = msgs[i];
								var dbmsg = filterMessage(msg);;
								var json = JSON.stringify(dbmsg);
								//console.log("unseen: "+json);
										//sendToUser(user,json);
								ws.send(json,sendcallback);

							}
						});

	return;
	case"messagelistfrom":
		var filter = {};
		filter["type"] = "message";
		filter["room"] = msg.room;
		if (msg.message_type)
			filter["message_type"] = msg.message_type;
		if (msg.from)
			filter["from"] = parseInt(msg.from);
		if(user.admin|| user.moderator)
		{
			if(msg.hasOwnProperty("visible"))
				filter["visible"] = msg.visible;
		}
		else
			filter["visible"]={$ne:false};
			var c = db.find(filter);
			c.sort({ts: -1});
			c.limit(40);
			c.toArray(function(err, msgs) {
									for (var i = msgs.length - 1; i >= 0; i--) {
										var msg = msgs[i];
										var dbmsg = filterMessage(msg);
										dbmsg["history"]=true;
										var json = JSON.stringify(dbmsg);
										if(msg.to)
										{
											var to=parseInt(msg.to);
											userExists(to,function(){
																		//sendToUser(user,json);
												ws.send(json,sendcallback);
											});
										}
										else
												//sendToUser(user,json);
											ws.send(json,sendcallback);

								}
							});
	return;
	case "messagehistory":
		var to=parseInt(msg.to);
		var from=parseInt(msg.from);
		var filter = {to:{$exists:true},$or:[{$and:[{to:to},{from:from}]},{$and:[{to:from},{from:to}]}]};
		filter["type"] = "message";
		if (msg.message_type)
			filter["message_type"] = msg.message_type;
		if(user.admin|| user.moderator)
		{
			if(msg.hasOwnProperty("visible"))
			filter["visible"] = msg.visible;
		}
		else
			filter["visible"]={$ne:false};
		var c = db.find(filter);
		c.sort({ts: -1});
		ws.cursor=c;
		c.nextObject(function(err, nextmsg) {
			if(err){
				logerr("cursor error");
				ws.cursor=null;
				logerr(err);
				var json = js({type:"refresh_cursor",reason:"cursor is null"});
				ws.send(json,sendcallback);
				return;
			}
			if(!nextmsg)return;
			if(msg.ts)
			if(nextmsg.ts>msg.ts){
				var json = js({type:"refresh_cursor",reason:"ts",ts:nextmsg.ts,msgts:msg.ts});
				ws.send(json,sendcallback);
				logerr("nextmsg.ts>msg.ts");
				return;
			}
			//var dbmsg = filterMessage(nextmsg);;
			var dbmsg = nextmsg
			dbmsg["pw"]=undefined;
			var return_type="nextmsg";
			dbmsg["type"]=return_type;
			var json = js(dbmsg);
			ws.send(json,sendcallback);
		});
		/*c.limit(10);
		c.toArray(function(err, msgs) {
								if(err){console.error(err);return;}
								for (var i = msgs.length - 1; i >= 0; i--) {
									var msg = msgs[i];
									var dbmsg = filterMessage(msg);
									var json = JSON.stringify(dbmsg);
											//sendToUser(user,json);
									ws.send(json);

								}
							});*/
	return;
	case "setname":
		if(msg.name){
			users.findOneAndUpdate({type: "user",id:user.id}, {$set: {name: msg.name}},{new: true},
			function(err,doc) {
				if(err){
					var json = JSON.stringify({type:"error",err:"nameexists"});
					//sendToUser(user,json);
					ws.send(json,sendcallback);
				}
				else{
					if(user.name){
						const nmsg=user.name+" => "+msg.name;
						sm(nmsg,user.id,ws.room,"name_change",user.id, user.name);
					}
					else
						sm(msg.name,user.id,user.room,"name_change",user.id,msg.name);
	
					user.name=msg.name;
					if(doc.value)
						doc.value["name"]=msg.name;
					ws.send(JSON.stringify({type: 'youAreOnline',doc:doc.value}),sendcallback);
					var json = JSON.stringify({type:"setname",name:msg.name});
					//sendToUser(user,json);
					ws.send(json,sendcallback);
				}
			});
		}
	return;
	case "seen":
		if(msg.id){
		var id;
		try{
			id=new mongodb.ObjectID(msg.id);
		}
		catch(e)
		{
			logerr(e);
		}
		db.findOneAndUpdate({_id:id}, {$set: {seen: true}},{new:true},
			function(err,doc) {
				if(err){
					logerr(err);
				}
				else{
					if(doc.to){}
					else{ 

						var seenMsg={type:"seen",id:msg.id};
						sendToRoom(doc.value.room,seenMsg);
					}
				}
			});
		}
	return;
	case "userprofile":
		if(msg.id){
			users.findOne({id: parseInt(msg.id)}, function(e, u) {
				if (u){
					var id=parseInt(msg.id);
					var _user=clients[id];
					var ips=[];
					if(_user){
						for (let c of _user.con) {
							if(c._socket)
								ips.push(c._socket.remoteAddress);
						}
						u["info"]=_user.info;
					}
					u["pw"]=undefined;
					u["type"]="userprofile";
					if(user.admin)
					u["ips"]=ips;
					var json = JSON.stringify(u);
					ws.send(json,sendcallback);
				}
			});
		}
	return;
	case "toggle_message":
		var id = msg.id;
		var filter={};
		var visible=msg.visible;
		if(!user.moderator)
			filter["from"]=parseInt(user.id);
		filter["ts"]={$gt:(Date.now()-3600000)};
		try{
			filter['_id']=new mongodb.ObjectID(id);
		}
		catch(err){
			logerr(err);
		}
		if(msg.lang)
			filter['lang']=msg.lang;
		db.update(filter, {$set: {visible: visible,toggled_from:user.id}},{}, function(err,n) 
										{
										if(n==0)return;
										var json = JSON.stringify({
type: 'toggle_message',
id: id,
visible:msg.visible,
from:user.id
});
										for (var cid in clients) {
										if (clients[cid] == undefined) continue;

										sendToUser(clients[cid],json);
										}
										});
	return;
	case "getrooms":
		db.aggregate([{$group:{_id:"$room",count:{$sum:1}}},{$sort:{count:-1}}])
		.toArray(function(err, rooms) {
			if(err)return;
			var msg={type:"rooms",rooms:rooms};
			var a=
			JSON.stringify(msg);
			ws.send(a,sendcallback);
		});
	return;
}
if (!user.moderator && !user.admin) return;
if (msg.type === "getlanguages") {
	db.distinct("lang",
		function(err,langs)
		{
			if(err)return;
			for (var i = langs.length - 1; i >= 0; i--) {
				var lang = langs[i];
				var json = JSON.stringify({type:"lang",lang:lang});
				//sendToUser(user,json);
				ws.send(json,sendcallback);
			}
		});
}
if (!user.admin) return;
if(user.admin===true)
switch(msg.type){
	case "saveNode":
		if(msg.path&&msg.set)
			nodes.findOneAndUpdate({path:msg.path}, {$set: {"value": msg.set.value}},{}, function(err,r) {});
	break;
	case "userinfo":
		var id = parseInt(msg.id);
		// broadcast message to all connected clients
		var type;
		var name = "";
		var admin = false;
		var version = 0;
		var room;
		var ts;
		if (clients[id] === undefined)
			type = "userisoff";
		else {
			type = "userison";
			name = clients[id].name;
			admin = clients[id].admin;
			version = clients[id].version;
			room = clients[id].room;
			ts = clients[id].ts;
		}
		var cache = [];
		var cons;
		if(clients[id])
			cons=JSON.stringify([...clients[id].con.values()], 
				function(key, value) {
					if (typeof value === 'object' && value !== null) 
					{
						if (cache.indexOf(value) !== -1) 
						{
							// Circular reference found, discard key
							return;
						}
						// Store value in our collection
						cache.push(value);
					}
					return value;
				});
		cache = null; // Enable garbage 
		var json = JSON.stringify({
			type: type,
			id: id,
			name: name,
			admin: admin,
			room: room,
			cons: cons,
			version: version,
			ts: ts
		});
		//sendToUser(user,json);
		ws.send(json,sendcallback);
	return;
	case "delete":
		var id = msg.id;
		var _id;
		try{
			_id=new mongodb.ObjectID(id);
		}
		catch(e){
			logerr(e);
			return;
		}
		db.deleteOne({
			_id: _id
		});
		for (var cid in clients) {
			if (clients[cid] == undefined) continue;
			var json = JSON.stringify({
				type: 'delete',
				id: id
		});
		sendToUser(clients[cid],json);
		}
	return;
	case "possible_mods":
		users.aggregate([{$match:{moderator:{$exists:false},msgratio:{$exists:true,$ne:NaN},name:{$exists:true}}},{$project:{messages:1,id:1,online_ts:1,created:1,name:1,msgratio:1}},{$sort:{online_ts:-1}},{$limit: 512},{$sort:{messages:-1}},{$limit:64},{$sort:{created:1}},{$limit:32},{$sort:{msgratio:1}},{$limit:8},{$sort:{messages:-1}}])
				.toArray(
					function(err, users) {
						if(err)return;
						var msg={type:"possible_mods",users:users};
						var a=
							JSON.stringify(msg);
						sm(a,user.id,user.room);
					});
	return;
	case "toggle_moderator":
		var id = parseInt(msg.id);
		var moderator=msg.moderator;
		users.update({type: "user",id:id}, {$set: {moderator: moderator}},{}, function(err,count) {
			if(err)return;
			if(moderator)
				log("toggle_moderator: "+id);
		});
		if(clients[id])
			clients[id].moderator=moderator;
	return;
	case "ban":
		if(!msg.id)return;
		var id = parseInt(msg.id);
		var until=Date.now()+msg.duration;
		users.update({id:id}, {$set: {ban: until}},{}, function(err,count) {
			log("ban: "+id);
		});
		if(clients[id])
			clients[id].ban=until;
		return;
	}
}

function onClose(con) {
	if (user == null) {
		return;
	}
	//console.error("onClose:"+user.con.has(con));
	disconnect(user,con);
}
}
