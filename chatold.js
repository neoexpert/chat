"use strict";

// Optional. You will see this name in eg. 'ps' or 'top' command
process.title = 'node-chat';

// Port where we'll run the websocket server
var webSocketsServerPort = 9043;
var fs = require('fs');
var bson = require('bson');
var mongodb = require('mongodb');
var BSON = new bson.BSONPure.BSON();
var Long = bson.BSONPure.Long;
var webSocketServer = require('websocket').server;
var http = require('http');
var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
// Connection URL
var url = 'mongodb://localhost:27017/chat';
var db;
// Use connect method to connect to the server
MongoClient.connect(url, function(err, mdb) {
  assert.equal(null, err);
  console.log("Connected successfully to server");
  db=mdb.collection("chat");
  startChat(db)
});
var maxID = 0;
var server = http.createServer(function(request, response) {
    // Not important for us. We're writing WebSocket server, not HTTP server
});
server.listen(webSocketsServerPort, function() {
    console.log((new Date()) + " Server is listening on port " + webSocketsServerPort);
});
var wsServer = new webSocketServer({
    // WebSocket server is tied to a HTTP server. WebSocket request is just
    // an enhanced HTTP request. For more info http://tools.ietf.org/html/rfc6455#page-6
    httpServer: server
});

function startChat(mdb){
db.findOne({
       id: "maxID"
    },
    function(err, doc) {
        maxID = doc.value;
        console.log(maxID);
    });

}
//var Datastore = require('nedb');
//var db = new Datastore({
//    filename: 'chat'
//});
// websocket and http servers
// list of currently connected clients (users)
var clients = [];

class User {
    constructor(con, id, admin, name, ts) {
        this.con = new Set();
        this.con.add(con);
        this.id = parseInt(id);
        this.admin = admin;
        this.name = name;
        this.ts = ts;
    }
}


function guid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
        s4() + '-' + s4() + s4() + s4();
}

function saveUser(id, pw) {
    db.insert({
        type: "user",
        id: id,
        pw: pw,
	created: +new Date()
    });
}

function sendLater(to, json) {}

function processUser(con, id, msg) {
    var user = null;
    if (clients[id] === undefined) {
        user = new User(con, 0, false, "", new Date() / 1000);
        clients[id] = user;
    } else {
        clients[id].con.add(con);
        user = clients[id];
    }

    for (var c in clients) {
        if (clients[c] === undefined)
            continue;
        if (clients[c].admin === true) {
            var json2 = JSON.stringify({
                type: 'adminison'
            });
            con.sendUTF(json2);
        }
    }

    console.log("New Login: id: " + id);

    clients[id].id = id;
    clients[id].name = msg.name;
    clients[id].version = msg.version;
    clients[id].room = msg.room;
    if (parseInt(msg.version) <= 40)
        if (clients[id].room) {
            var c = db.find({
                $and: [{
                    type: "message"
                }, {
                    room: clients[id].room
                }, {
                    message_type: "text"
                }]
            });
            c.sort({
                ts: -1
            });
            c.limit(20);
            c.toArray(function(err, msgs) {
                for (var i = msgs.length - 1; i >= 0; i--) {
                    var msg = msgs[i];
                    var dbmsg = {};
                    dbmsg["type"] = 'usermsg';
                    dbmsg["_id"] = msg._id;
                    dbmsg["name"] = msg.name;
                    dbmsg["message"] = msg.message;
                    dbmsg["message_type"] = msg.message_type;
                    dbmsg["from"] = msg.from;
                    dbmsg["room"] = msg.room;
                    if (msg.additional)
                        dbmsg["additional"] = msg.additional;
                    if (msg.refer)
                        dbmsg["refer"] = msg.refer;
                    if (msg.lang)
                        dbmsg["lang"] = msg.lang;

                    var json = JSON.stringify(dbmsg);
                    con.sendUTF(json);
                }
            });
        }
    var json = JSON.stringify({
        type: 'userison',
        name: msg.name,
        id: id,
        admin: user.admin,
        room: user.room,
        version: user.version
    });
    for (var cid in clients) {
        if (cid == id) continue;
        if (clients[cid] == undefined) continue;
        //console.log(JSON.stringify(client));
        for (let c of clients[cid].con) {
            c.sendUTF(json);
            var json2 = JSON.stringify({
                type: 'userison',
                name: clients[cid].name,
                id: cid,
                admin: clients[cid].admin,
                room: clients[cid].room,
                version: clients[cid].version
            });
            con.sendUTF(json2);
        }
    }

    return user;
}

function processMessage(msg, user) {
    if(user.ban>+new Date())
	return;
    if(msg.message_type=="text")
    	db.update({type: "user",id:user.id}, {$inc: {"messages": 1}},{}, function() {});
    if(msg.message_type=="rocket")
    	db.update({type: "user",id:user.id}, {$inc: {"rockets": 1}},{}, function() {});
    db.update({_id: new mongodb.ObjectID(user._id)}, {$set: {"messages": +new Date()}}, {}, function() {});
    if (user.room != undefined) {
        msg["type"] = "usermsg";
        var json = JSON.stringify(msg);
        for (var cid in clients) {
            if (clients[cid] == undefined) continue;
            if (user.room === clients[cid].room)
                for (let c of clients[cid].con) {
                    c.sendUTF(json);
                }

        }
        return;
    }
    var to = parseInt(msg.to);
    userExists(to, function() {
	console.log("userexists: "+to);
        var json = JSON.stringify({
            type: 'usermsg',
            name: msg.name,
            message: msg.message,
            message_type: msg.message_type,
            from: to,
            myown: true
        });
        for (let c of user.con)
            c.sendUTF(json);

        json = JSON.stringify({
            type: 'usermsg',
            name: msg.name,
            message: msg.message,
            message_type: msg.message_type,
            from: user.id
        });
        //for (var cid in clients) {
        //if(parseInt(cid)===to)
        //{
        if (clients[to] !== undefined)
            for (let c of clients[to].con) {
                c.sendUTF(json);
            }
        else
            sendLater(to, json);
        //}
        //}
    });
}

function userExists(id, callback) {
    db.findOne({
        id: id
    }, function(e, u) {
        if (u != null)
            callback();
    });
}
var lastRegisterTS=0;;
function checkUser(con, msg, callback) {
    var id = parseInt(msg.myID);
    var pw = msg.pw;
    if (id == 0 || isNaN(id)) {
	var cts=+new Date();
	console.log(cts-lastRegisterTS);
		if(cts-lastRegisterTS<20000){
			callback("wait",null);
			return;
		}
		lastRegisterTS=+new Date();
		maxID++;
		id = maxID;
		db.update({
		    id: "maxID"
		}, {
		    $set: {
			"value": maxID
		    }
		}, {}, function() {});
		var pw = guid();
		var json2 = JSON.stringify({
		    type: 'yourNewID',
		    id: id,
		    pw: pw
		});
		con.sendUTF(json2);
		console.log(json2);
		saveUser(id, pw);
		var user = processUser(con, id, msg);
		callback(null,user);
	    } else {
		db.findOne({
		    type: "user",
		    id: id
		}, function(e, u) {
		    if (u == null)
			callback("wrongid",null);
		    else if (u.pw === pw) {
			var user = processUser(con, id, msg);
			try{
			db.update({_id: new mongodb.ObjectID(u._id)}, {$set: {"online_ts": +new Date()}}, {}, function() {});
			}catch(e){}
			user.moderator=u.moderator;
			user.ban=u.ban;
			if (u.moderator) {
			    var json = JSON.stringify({
				type: 'youaremoderator',
				id: u.id
			    });
			    con.sendUTF(json);
			}
			if (u.ban>+new Date()) {
			    var json = JSON.stringify({
				type: 'youarebanned',
				id: u.id
			    });
			    con.sendUTF(json);
			}
			user.admin = u.admin;
			if (u.admin) {
			    var json = JSON.stringify({
				type: 'youareadmin',
				id: u.id
			    });
			    con.sendUTF(json);
			    var json2 = JSON.stringify({
				type: 'adminison'
			    });
			    for (var cid in clients) {
				if (clients[cid] === undefined) continue;
				for (let c of clients[cid].con)
				    c.sendUTF(json2);
			    }
			}
			callback(null,user);
		    } else
			{
				console.log(u.pw+"!="+pw+"=="+(u.pw === pw));
				callback("wrongpw",null);
			}
		});
	    }

	}


	// This callback function is called every time someone
	// tries to connect to the WebSocket server
	wsServer.on('request', onRequest);

	function onRequest(request) {

	    //console.log((new Date()) + ' Connection from origin ' + request.origin + '.');

	    // accept connection - you should check 'request.origin' to make sure that
	    // client is connecting from your website
	    // (http://en.wikipedia.org/wiki/Same_origin_policy)
	    var connection = request.accept(null, request.origin);

	    var user = null;

	    //console.log((new Date()) + ' Connection accepted.');

	    connection.on('message', onMessage);


	    // user disconnected
	    connection.on('close', onClose);

	    var json = JSON.stringify({
		type: 'yourID'
	    });
	    connection.sendUTF(json);
	    var mc=0;
	    var lastMsgTS=0;
	    function onMessage(message) {
		 var dt=+new Date() - lastMsgTS;
		 if (dt > 10000)
	        mc = 0;
         if (mc == 0)
                lastMsgTS = +new Date();

	 if (mc > 15)
	 {
                console.log("spamfilter");
                var json = JSON.stringify({
		      type:"error",
                      err: "wait "+Math.ceil((10000-dt)/1000)+" s"
                });
                connection.sendUTF(json);
		if(user==null)
                    connection.close();
                    
		lastMsgTS += 1000;
	        return;
	 }
	 mc++;
        if (message.type === 'binary') {
            console.log(message.binaryData);
            console.log(JSON.stringify(message.binaryData));
            try {
                var msg = BSON.deserialize(message.binaryData);
                console.log('msg:', msg);
                var to = parseInt(msg.to);
                console.log("binary to: " + to);
                msg.from = user.id;
                var data = BSON.serialize(msg, false, true, false);
                var it = clients[to].con.values();
                var con = it.next();
                //console.log(con)
                con.value.sendBytes(data);
            } catch (e) {
                console.log(e);
            }
        }

        if (message.type === 'utf8') { // accept only text
            console.log(message.utf8Data);
	    console.log("l:"+ message.utf8Data.length);
	    if(message.utf8Data.length>125)
		mc+=4;
            try {
                var msg = JSON.parse(message.utf8Data);
            } catch (e) {
                console.log(e.message);
		connection.close();
                return;
            }
            if (msg.type === "myID") {

                checkUser(connection, msg, 
		function(err,newUser) {
		    if(err){
                        var json = JSON.stringify({
			    type:"error",
                            err: err
                        });
                        connection.sendUTF(json);
                        connection.close();
			return;
		    }
                    if (newUser === null)
                        connection.close();
                    else
                        user = newUser;
                });
                return;

            }
            if (user === null) return;
            if (msg.type === "usermsg") {
                var dbmsg = {};
                dbmsg["type"] = 'message';
                dbmsg["_id"] = msg._id;
                dbmsg["name"] = msg.name;
                dbmsg["message"] = msg.message;
                dbmsg["message_type"] = msg.message_type;
                dbmsg["to"] = msg.to;
                dbmsg["from"] = user.id;
                dbmsg["room"] = user.room;
                dbmsg["ts"] = +new Date();
                if (msg.additional)
                    dbmsg["additional"] = msg.additional;
                if (msg.refer)
                    dbmsg["refer"] = msg.refer;
                if (msg.lang)
                    dbmsg["lang"] = msg.lang;
                if (msg.country)
                    dbmsg["country"] = msg.country;

                db.insert(dbmsg);
                processMessage(dbmsg, user);
            }
            if (msg.type === "userlist") {
                for (var cid in clients) {
                    if (cid == user.id) continue;
                    if (clients[cid] == undefined) continue;
                    //console.log(JSON.stringify(client));

                    var json = JSON.stringify({
                        type: 'userison',
                        name: clients[cid].name,
                        id: cid,
                        admin: clients[cid].admin,
                        room: clients[cid].room,
                        version: clients[cid].version
                    });
                    for (let c of user.con)
                        c.sendUTF(json);
                }
            }
            if (msg.type === "messagelist") {
                var filter = {};
                filter["type"] = "message";
                filter["room"] = msg.room;
                if (msg.lang)
                  filter["lang"] = msg.lang;
                if (msg.message_type)
                  filter["message_type"] = msg.message_type;
                else
                  filter["message_type"] = "text";
                if(user.admin|| user.moderator)
                {
                  if(msg.hasOwnProperty("visible"))
                      filter["visible"] = msg.visible;
                }
                else
                    filter["visible"]={$ne:false};
                var c = db.find(
                    filter
                );
                c.sort({
                    ts: -1
                });
                c.limit(20);
                c.toArray(function(err, msgs) {
                    for (var i = msgs.length - 1; i >= 0; i--) {
                        var msg = msgs[i];
                        var dbmsg = {};
                        dbmsg["type"] = 'usermsg';
                        dbmsg["_id"] = msg._id;
                        dbmsg["name"] = msg.name;
                        dbmsg["message"] = msg.message;
                        dbmsg["message_type"] = msg.message_type;
                        dbmsg["from"] = msg.from;
                        dbmsg["room"] = msg.room;
                        if (msg.additional)
                            dbmsg["additional"] = msg.additional;
                        if (msg.refer)
                            dbmsg["refer"] = msg.refer;
                        if (msg.lang)
                            dbmsg["lang"] = msg.lang;
                        if (msg.country)
                            dbmsg["country"] = msg.country;
                        if (msg.hasOwnProperty("visible"))
                            dbmsg["visible"] = msg.visible;
                        if (msg.toggled_from)
                            dbmsg["toggled_from"] = msg.toggled_from;

                        var json = JSON.stringify(dbmsg);
                        for (let c of user.con)
                            c.sendUTF(json);

                    }
                });

            }
            if (!user.moderator && !user.admin) return;
            if (msg.type === "toggle_message") {
                var id = msg.id;
		var filter={};
		var visible=msg.visible;
		filter['_id']=new mongodb.ObjectID(id);
		if(msg.lang)
			filter['lang']=msg.lang;
		console.log(filter);
    	        db.update(filter, {$set: {visible: visible,toggled_from:user.id}},{}, function() {});
                var json = JSON.stringify({
                    type: 'toggle_message',
                    id: id,
		    visible:msg.visible,
                    from:user.id
                });
                for (var cid in clients) {
                    if (clients[cid] == undefined) continue;
                    //console.log(JSON.stringify(client));

                    for (let c of clients[cid].con)
                        c.sendUTF(json);
                }
            }
            if (!user.admin) return;
            if (msg.type === "userinfo") {
                var id = parseInt(msg.id);
                // broadcast message to all connected clients
                var type;
                var name = "";
                var admin = false;
                var version = 0;
                var room;
                var ts;
                if (clients[id] === undefined)
                    type = "userisoff";
                else {
                    type = "userison";
                    name = clients[id].name;
                    admin = clients[id].admin;
                    version = clients[id].version;
                    room = clients[id].room;
                    ts = clients[id].ts;
                }
                var cache = [];
		var cons;
		if(clients[id])
                cons=JSON.stringify([...clients[id].con.values()], 
                function(key, value) {
                   if (typeof value === 'object' && value !== null) 
                   {
                      if (cache.indexOf(value) !== -1) 
                      {
                         // Circular reference found, discard key
                         return;
                      }
                      // Store value in our collection
                      cache.push(value);
                   }
                   return value;
               });
               cache = null; // Enable garbage 
                var json = JSON.stringify({
                    type: type,
                    id: id,
                    name: name,
                    admin: admin,
                    room: room,
                    cons: cons,
                    version: version,
                    ts: ts
                });
                for (let c of user.con)
                    c.sendUTF(json);
            }
            if (msg.type === "delete") {
                var id = msg.id;
		console.log(id);
		var _id;
		try{
			_id=new mongodb.ObjectID(id);
		}
		catch(e){
			console.error(e);
			return;
		}
                db.deleteOne({
                    _id: _id
                });
                for (var cid in clients) {
                    if (clients[cid] == undefined) continue;
                    //console.log(JSON.stringify(client));

                    var json = JSON.stringify({
                        type: 'delete',
                        id: id
                    });
                    for (let c of clients[cid].con)
                        c.sendUTF(json);
                }
            }
            if (msg.type === "toggle_moderator") {
                var id = parseInt(msg.id);
                var moderator=msg.moderator;
                console.log("moderator:"+id);
    	        db.update({type: "user",id:id}, {$set: {moderator: moderator}},{}, function(err,count) {
console.log(count);
});
            }
            if (msg.type === "ban") {
                var id = parseInt(msg.id);
                var duration=+new Date()+msg.duration;
		user.ban=ban;
    	        db.update({type: "user",id:id}, {$set: {ban: duration}},{}, function(err,count) {
console.log(count);
});
            }


        }
    }

    function onClose(con) {
        if (user == null) {
            console.log("user null disconnected");
            return;
        }

        //console.log((new Date()) + " Peer " +
          //  connection.remoteAddress + " disconnected.");

        //var index= user.con.indexOf(connection);
        //if (index > -1) {
        //	user.con.splice(index, 1);
        //}
        user.con.delete(connection);


        console.log("connections: " + user.con.size);
        // remove user from the list of connected clients
        if (user.con.size == 0) {
            clients[user.id] = undefined;

            console.log("user " + user.id + " disconnected");
            var json = JSON.stringify({
                type: 'userisoff',
                id: user.id,
                room: user.room
            });
            for (var cid in clients) {
                if (clients[cid] === undefined) continue;
                if (cid == user.id) continue;
                for (let c of clients[cid].con)
                    c.sendUTF(json);
                //for(var c in clients[cid].con)
                //{
                //        clients[cid].con[c].sendUTF(json);
                //}
            }

        }
        // push back user's color to be reused by another user

    }
}
