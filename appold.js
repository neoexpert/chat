"use strict";

// Optional. You will see this name in eg. 'ps' or 'top' command
process.title = 'node-chat';

// Port where we'll run the websocket server
var webSocketsServerPort = 80;
var fs = require('fs');
var path = require('path');
var bson = require('bson');
var mongodb = require('mongodb');
var BSON = new bson.BSONPure.BSON();
var Long = bson.BSONPure.Long;
var util = require('util');
var webSocketServer = require('websocket').server;
var http = require('http');
var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var bot = require('./bot');
// Connection URL
var url = 'mongodb://localhost:27017/chat';
var db;
var users;
var factsdb;
var facts={};
function js(j){
	return JSON.stringify(j);
}
facts.update=function(s,fact,inc){
console.log(fact);
factsdb.update(s,{$set:fact,$inc:{count:inc}},{upsert:true});
}
facts.find=function(f,callback){
	var c=factsdb.find(f);
	c.sort({count:-1});
	c.limit(10);
        c.toArray(
	function(err, msgs) {
		if(err)return;
		if(msgs.length<1)return;
		callback(msgs);
	});
}
/*facts.findOne=function(f,callback){
	var c=factsdb.find(f);
	c.sort({count:-1});
	c.limit(1);
        c.toArray(
	function(err, msgs) {
		if(err)return;
		if(msgs.length>0)
		callback(msgs[0]);
	});
}*/
facts.remove=function(f,callback){
	factsdb.deleteOne(f);	
}
facts.findOne=function(_f,_callback){
	let f=_f;
	let callback=_callback;
	var c=factsdb.find(f);
	c.sort({count:-1});
	c.limit(1);
        c.toArray(
	function(err, msgs) {
		if(err){
			console.error(err);
			return
		};
		if(msgs.length>0){
		callback(msgs[0]);
		}
		else
		callback();
		
	});
}

// Use connect method to connect to the server
MongoClient.connect(url, function(err, mdb) {
  assert.equal(null, err);
  console.log("Connected successfully to server");
  mdb.authenticate("neoexpert", "1cneo2005", function(err, res) {
  // callback
  db=mdb.collection("chat");
  users=mdb.collection("users");
  factsdb=mdb.collection("facts");
  startChat(db)
});
});
var maxID = 0;
var server = http.createServer(function(request, response) {
response.writeHead (301, {'Location': 'https://neochat.tk'});

response.end();
return;
var filePath = './html' + request.url;
filePath=filePath.replace(/\.\./g,"");
console.log("GET "+filePath);
    if(filePath=='./html/info.js'){
	response.writeHead(200, { 'Content-Type': "text/javascript" });
       	response.end("var country='de';", 'utf-8');
	return;
    } 

    var extname = path.extname(filePath);
    var contentType = 'text/html';
    switch (extname) {
        case '.js':
            contentType = 'text/javascript';
            break;
        case '.css':
            contentType = 'text/css';
            break;
        case '.txt':
            contentType = 'text/plain';
            break;
        case '.json':
            contentType = 'application/json';
            break;
        case '.png':
            contentType = 'image/png';
            break;      
        case '.jpg':
            contentType = 'image/jpg';
            break;
        case '.wav':
            contentType = 'audio/wav';
            break;
        case '.mp3':
            contentType = 'audio/mpeg';
            break;
	default:
    		if (filePath == './html/')
        		filePath = './html/index.html';
    }
    if(!fs.existsSync(filePath))
	    filePath = './html/chat.html';
    fs.readFile(filePath, function(error, content) {
        if (error) {
            if(error.code == 'ENOENT'){
                fs.readFile('./404.html', function(error, content) {
                    response.writeHead(200, { 'Content-Type': contentType });
                    response.end(content, 'utf-8');
                });
            }
            else {
                response.writeHead(500);
                response.end('Sorry, check with the site admin for error: '+error.code+' ..\n');
                response.end(); 
            }
        }
        else {
            response.writeHead(200, { 'Content-Type': contentType });
            response.end(content, 'utf-8');
        }
    });
});
server.listen(webSocketsServerPort, function() {
    console.log((new Date()) + " Server is listening on port " + webSocketsServerPort);
	process.setgid('alex');
	process.setuid('alex');
});
var wsServer = new webSocketServer({
    // WebSocket server is tied to a HTTP server. WebSocket request is just
    // an enhanced HTTP request. For more info http://tools.ietf.org/html/rfc6455#page-6
    httpServer: server
});

function startChat(mdb){
db.findOne({
       id: "maxID"
    },
    function(err, doc) {
        maxID = doc.value;
        console.log(maxID);
    });
	sm("Chat started...",0,"scdown");
	sm("Chat started...",0,"dating");
	

}
//var Datastore = require('nedb');
//var db = new Datastore({
//    filename: 'chat'
//});
// websocket and http servers
// list of currently connected clients (users)
var clients = [];

class User {
    constructor(con, id, admin, name, ts) {
        this.con = new Set();
        this.con.add(con);
        this.id = parseInt(id);
        this.admin = admin;
        this.name = name;
        this.ts = ts;
    }
}

var max=1000000000;
var min=1000000;

function beat(){
	var t=Math.random() * (max - min) + min;
	setTimeout(beat, t);
}
String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};
function processBot(umsg,user){
return function(){
	var msg=umsg.message;
	if((typeof msg)!="string")
		return;
	bot.process(facts,msg,user,function(a){
		sm(a,user.id,user.room);
	});
		if(msg.refer==="0")
		{
			
			if(msg.length<100)
			{
				msg=msg.replace(/<span style=\"color:#bbd43d;\">s/g, msg.name);
				msg=msg.replace(/ich/g, 'Du');
				sm(msg,user.id,user.room);
			}
			return;
		}
		if(msg.indexOf("mod") > -1)	
		{
			if(msg.indexOf("kann")>-1){
				sm("nein",user.id,user.room);
				return;
			}
			if(msg.indexOf("darf")>-1){
				sm("nein",user.id,user.room);
				return;
			}
		}
	}
}
function sm(msg,refer,room){
        var dbmsg = {
            type: 'message',
            name: "S",
            message: msg,
            message_type: "text",
            from: 0,
	    room:room
        };
	dbmsg["ts"] = +new Date();
	if(refer)
		dbmsg["refer"] = refer;
        db.insert(dbmsg,function(err, record){
       		dbmsg["type"] = "usermsg";
        	for (var cid in clients) {
            	if (clients[cid] == undefined) continue;
            	if (room === clients[cid].room)
			sendToUser(clients[cid],JSON.stringify(dbmsg));
        	}
	});
}

function guid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
        s4() + '-' + s4() + s4() + s4();
}

function saveUser(id, pw) {
    users.insert({
        type: "user",
        id: id,
        pw: pw,
	created: +new Date()
    });
}

function sendLater(to, json) {}

function processUser(con, id, msg) {
    var user = null;
    con.room=msg.room;
    if (clients[id] === undefined) {
        user = new User(con, 0, false, "", new Date() / 1000);
        clients[id] = user;
    } else {
        clients[id].con.add(con);
        user = clients[id];
    }


    console.log("New Login: id: " + id);

    clients[id].id = id;
    clients[id].name = msg.name;
    clients[id].version = msg.version;
    clients[id].room = msg.room;
    var json = JSON.stringify({
        type: 'userison',
        name: msg.name,
        id: id,
        admin: user.admin,
        room: user.room,
        version: user.version
    });
    for (var cid in clients) {
        if (cid == id) continue;
        if (clients[cid] == undefined) continue;
        //console.log(JSON.stringify(client));
            var json2 = JSON.stringify({
                type: 'userison',
                name: clients[cid].name,
                id: cid,
                admin: clients[cid].admin,
                room: clients[cid].room,
                version: clients[cid].version
            });
            con.sendUTF(json2);
	    sendToUser(clients[cid],json,con.room);
    }

    return user;
}
function disconnect(user,con){
        user.con.delete(con);


        console.log("connections: " + user.con.size);
        // remove user from the list of connected clients
        if (user.con.size == 0) {
            clients[user.id] = undefined;

            console.log("user " + user.id + " disconnected");
            var json = JSON.stringify({
                type: 'userisoff',
                id: user.id,
                room: user.room
            });
            for (var cid in clients) {
                if (clients[cid] === undefined) continue;
                if (cid == user.id) continue;
		sendToUser(clients[cid],json);
                //for(var c in clients[cid].con)
                //{
                //        clients[cid].con[c].sendUTF(json);
                //}
            }

        }
}
function sendToRoom(room,msg){
        var json = JSON.stringify(msg);
	console.log("toRoom: "+json);
        for (var cid in clients) {
            if (clients[cid] == undefined) continue;
            //if (room === clients[cid].room)
		sendToUser(clients[cid],json,room);

        }
}
function sendToUser(user,msg,room){
	if(user)
	for (let c of user.con) {
		if(c.connected){
		if(!room||c.room==room)
        		c.sendUTF(msg);
		}
		else
		{
			console.error(user.id+" was not connected. Connections: "+user.con.size);
			console.error("c:"+c);
			disconnect(user,c);
		}
        }
}
function filterMessage(msg){
	var dbmsg = {};
	dbmsg["type"] = 'usermsg';
	dbmsg["_id"] = msg._id;
	dbmsg["name"] = msg.name;
	dbmsg["message"] = msg.message;
	dbmsg["message_type"] = msg.message_type;
	dbmsg["from"] = msg.from;
	dbmsg["room"] = msg.room;
	dbmsg["ts"] = msg.ts;
	if (msg.to)
	    dbmsg["to"] = msg.to;
	if (msg.refer)
	    dbmsg["refer"] = msg.refer;
	if (msg.lang)
	    dbmsg["lang"] = msg.lang;
	if (msg.country)
	    dbmsg["country"] = msg.country;
	if (msg.hasOwnProperty("visible"))
	    dbmsg["visible"] = msg.visible;
	if (msg.toggled_from)
	    dbmsg["toggled_from"] = msg.toggled_from;
	if (msg.color)
	    dbmsg["color"] = msg.color;
	if (msg.ts)
	    dbmsg["ts"] = msg.ts;
	if (msg.admin)
	    dbmsg["admin"] = msg.admin;
	if (msg.moderator)
	    dbmsg["moderator"] = msg.moderator;
	if (msg.baned)
	    dbmsg["baned"] = msg.baned;
	if (msg.seen)
	    dbmsg["seen"] = msg.seen;
	if (msg.x&&msg.y)
	{
	    dbmsg["x"] = msg.x;
	    dbmsg["y"] = msg.y;
	}
	return dbmsg;
}
function processMessage(msg, user,con) {
    if(msg.message_type=="text")
    	users.update({type: "user",id:user.id}, {$inc: {"messages": 1}},{}, function() {});
    if(msg.message_type=="rocket")
    	users.findOneAndUpdate({type: "user",id:user.id}, {$inc: {"rockets": 1}},{}, function(err,r) {
if(err)
	return;
	if(!r.value)return;
	var r=r.value.rockets;
	if(r==100||r==1000||r==5000||r==10000||r==50000||r==100000)
		sm(user.name+": "+r+" rockets",user.id,user.room);
	//console.log("ROCKETS: "+r.value.rockets);
});
    //db.update({_id: new mongodb.ObjectID(user._id)}, {$set: {"messages": +new Date()}}, {}, function() {});
    if (con.room != undefined) {
        msg["type"] = "usermsg";
        var json = JSON.stringify(msg);
	if(msg.to)
	{
		sendToUser(user,json,con.room);
		if(msg.baned)return;
		var to=parseInt(msg.to);
		userExists(to,function(){
			sendToUser(clients[to],json,con.room);
		});
	}
	else
        for (var cid in clients) {
            if (clients[cid] == undefined) continue;
            //if (user.room === clients[cid].room)
		if(!msg.baned||clients[cid].admin||cid==user.id)
			sendToUser(clients[cid],json,con.room);

        }
        return;
    }
    /*var to = parseInt(msg.to);
    userExists(to, function() {
        var json = JSON.stringify({
            type: 'usermsg',
            name: msg.name,
            message: msg.message,
            message_type: msg.message_type,
            from: to,
            myown: true
        });
	sendToUser(user,json,con.room);

        json = JSON.stringify({
            type: 'usermsg',
            name: msg.name,
            message: msg.message,
            message_type: msg.message_type,
            from: user.id
        });
        //for (var cid in clients) {
        //if(parseInt(cid)===to)
        //{
        if (clients[to] !== undefined)
	    sendToUser(clients[to],json);
        else
            sendLater(to, json);
        //}
        //}
    });*/
}

function userExists(id, callback) {
    users.findOne({
        id: id
    }, function(e, u) {
        if (u != null)
            callback();
    });
}
var lastRegisterTS=0;;
function checkUser(con, msg, callback) {
    var id = parseInt(msg.myID);
    var pw = msg.pw;
    if (id == 0 || isNaN(id)) {
	var cts=+new Date();
		if(cts-lastRegisterTS<10000){
			callback("wait",null);
			return;
		}
		lastRegisterTS=+new Date();
		maxID++;
		id = maxID;
		db.update({
		    id: "maxID"
		}, {
		    $set: {
			"value": maxID
		    }
		}, {}, function() {});
		var pw = guid();
		var json2 = JSON.stringify({
		    type: 'yourNewID',
		    id: id,
		    pw: pw
		});
		con.sendUTF(json2);
		console.log(json2);
		saveUser(id, pw,con);
    		con.sendUTF(JSON.stringify({type: 'youAreOnline'}));
		var user = processUser(con, id, msg);
		callback(null,user);
	    } else {
		users.findOne({
		    type: "user",
		    id: id
		}, function(e, u) {
		    if (u == null)
			callback("wrongid",null);
		    else if (u.pw === pw) {
			var user = processUser(con, id, msg);
    			con.sendUTF(JSON.stringify({type: 'youAreOnline',doc:u}));
			db.count({from:u.id,visible:false,toggled_from:{$ne:u.id}},
			function(err,count){
			if(err)return;	
			users.update({id:id}, {$set: {"hiddenmsgs":count}}, {}, function() {});

			});
			var toset={"online_ts": +new Date(),msgratio:u.hiddenmsgs/u.messages}
			if(msg.color)
			toset["color"]=msg.color;
			users.update({id:id}, {$set: toset}, {}, function() {});
			user.moderator=u.moderator;
			user.ban=u.ban;
			user.color=u.color;
			if (u.moderator) {
			    var json = JSON.stringify({
				type: 'youaremoderator',
				id: u.id
			    });
			    con.sendUTF(json);
			}
			if (u.ban>+new Date()) {
			    var json = JSON.stringify({
				type: 'youarebanned',
				id: u.id
			    });
			    con.sendUTF(json);
			}
			user.admin = u.admin;
			if (u.admin) {
			    var json = JSON.stringify({
				type: 'youareadmin',
				id: u.id
			    });
			    con.sendUTF(json);
			}
			user.name=u.name;
			callback(null,user);
		    } else
			{
				callback("wrongpw",null);
			}
		});
	    }

	}


	// This callback function is called every time someone
	// tries to connect to the WebSocket server
	wsServer.on('request', onRequest);

	function onRequest(request) {

	    //console.log((new Date()) + ' Connection from origin ' + request.origin + '.');

	    // accept connection - you should check 'request.origin' to make sure that
	    // client is connecting from your website
	    // (http://en.wikipedia.org/wiki/Same_origin_policy)
	    var connection = request.accept(null, request.origin);

	    var user = null;

	    //console.log((new Date()) + ' Connection accepted.');

	    connection.on('message', onMessage);
	    connection.on('error', onError);


	    // user disconnected
	    connection.on('close', onClose);

	    var json = JSON.stringify({
		type: 'yourID'
	    });
	    connection.sendUTF(json);
	    var mc=0;
	    var lastMsgTS=0;
	    function onError(message) {}
	    function onMessage(message) {
		 var dt=+new Date() - lastMsgTS;
		 if (dt > 10000)
	        mc = 0;
         if (mc == 0)
                lastMsgTS = +new Date();

	 if (mc > 35)
	 {
                console.log("spamfilter ("+user.name+" ID: "+user.id+")");
                var json = JSON.stringify({
		      type:"error",
                      err: "wait "+Math.ceil((10000-dt)/1000)+" s"
                });
                connection.sendUTF(json);
		if(user==null)
                    connection.close();
                    
		lastMsgTS += 1000;
		if(user.admin!=true)
	        return;
	 }
	 mc++;
        if (message.type === 'binary') {
            try {
                var msg = BSON.deserialize(message.binaryData);
                var to = parseInt(msg.to);
                console.log("binary to: " + to);
                msg.from = user.id;
                var data = BSON.serialize(msg, false, true, false);
                var it = clients[to].con.values();
                var con = it.next();
                //console.log(con)
                con.value.sendBytes(data);
            } catch (e) {
                console.log(e);
            }
        }

        if (message.type === 'utf8') { // accept only text
            console.log(message.utf8Data);
	    if(message.utf8Data.length>125)
		mc+=4;
            try {
                var msg = JSON.parse(message.utf8Data);
            } catch (e) {
                console.log(e.message);
		connection.close();
                return;
            }
            if (msg.type === "myID") {

                checkUser(connection, msg, 
		function(err,newUser) {
		    if(err){
                        var json = JSON.stringify({
			    type:"error",
                            err: err
                        });
                        connection.sendUTF(json);
                        connection.close();
			return;
		    }
                    if(newUser === null)
                        connection.close();
                    else
                        user = newUser;
                });
                return;

            }
            if (user === null) return;
            if (msg.type === "usermsg") {
                var dbmsg = {};
                dbmsg["type"] = 'message';
                dbmsg["_id"] = msg._id;
		if(user.name)
			dbmsg["name"] = user.name;
		else
                	dbmsg["name"] = msg.name;
                dbmsg["message"] = msg.message;
                dbmsg["message_type"] = msg.message_type;
		if(msg.to)
                	dbmsg["to"] = parseInt(msg.to);
                dbmsg["from"] = user.id;
                dbmsg["room"] = connection.room;
                dbmsg["ts"] = +new Date();
                if (msg.refer)
                    dbmsg["refer"] = msg.refer;
                if (msg.lang)
                    dbmsg["lang"] = msg.lang;
                if (msg.country)
                    dbmsg["country"] = msg.country;
                if (user.moderator)
                    dbmsg["moderator"] = true;
                if (user.admin)
                    dbmsg["admin"] = true;
                if (user.color)
                    dbmsg["color"] = user.color;
    		var baned=user.ban>+new Date();
		if(baned)
			dbmsg["baned"]=baned;
		if(msg.x&&msg.y)
		{
                    dbmsg["x"] = msg.x;
                    dbmsg["y"] = msg.y;
		}

                db.insert(dbmsg,function(err, record){
			connection.sendUTF(JSON.stringify({type:"received",message_type:msg.message_type,to:msg.to}));
                	processMessage(dbmsg, user,connection);
			if(!dbmsg.to&&!msg.baned)
			setTimeout(processBot(dbmsg,user), 1);
		});
            }
            if (msg.type === "userlist") {
                for (var cid in clients) {
                    if (cid == user.id) continue;
                    if (clients[cid] == undefined) continue;
                    //console.log(JSON.stringify(client));

                    var json = JSON.stringify({
                        type: 'userison',
                        name: clients[cid].name,
                        id: cid,
                        admin: clients[cid].admin,
                        room: clients[cid].room,
                        version: clients[cid].version
                    });
		    //sendToUser(user,json);
		    connection.sendUTF(json);
                }
            }
            if (msg.type === "updateme") {
		var data=msg.data;
		if(data.id)return;
    	        users.findOneAndUpdate({type: "user",id:user.id}, {$set: data},{returnOriginal:false},
		function(err,doc) {
			if(err){
				var json = JSON.stringify({type:"error",err:"nameexists"});
				//sendToUser(user,json);
				connection.sendUTF(json);
			}
			else{
				var data={type: 'userupdate',doc:doc.value};
				sendToRoom(connection.room,data);
			}
		});
            }
            if (msg.type === "findOne") {
		if(!msg.filter)return;
		var filter=msg.filter;
		users.findOne(filter, function(e, u) {
        		if (u){
				var r={};
				u["pw"]=undefined;
				r["type"]=msg.return_type;
				r["doc"]=u;
				var json = JSON.stringify(r);
		    		connection.sendUTF(json);
			}
		});
	    }
            if (msg.type === "find") {
		if(!msg.filter)return;
		var filter=msg.filter;
		connection.cursor=
		db.find(
                    filter
                );
                connection.cursor.sort({ts: -1});
                var c = db.find(
                    filter
                );
	    }
            if (msg.type === "next") {
		mc--;
		if(!connection.cursor){return;console.log("cursor is null");}
		console.log("next:"+msg.ts);
      		connection.cursor.nextObject(function(err, nextmsg) {
		if(err){
			console.error(err);
			return;
		}
		if(!nextmsg)return;
		if(nextmsg.ts>msg.ts){
                	var json = JSON.stringify({type:"refresh_cursor"});
			connection.sendUTF(json);
			console.error("nextmsg.ts>msg.ts");
			return;
		}
                var dbmsg = filterMessage(nextmsg);;
		dbmsg["type"]="nextmsg";
                var json = JSON.stringify(dbmsg);
		connection.sendUTF(json);

      		});
	    }
            if (msg.type === "findsome") {
		if(!msg.filter)return;
                var filter = msg.filter;
                filter["type"] = "message";
		console.log(filter);
		connection.cursor=
		db.find(
                    filter
                );
                var c = db.find(
                    filter
                );
                c.sort({ts: -1});
                c.limit(20);
                c.toArray(function(err, msgs) {
		    if(err){
			console.error(err);
			return;
                    }
                    for (var i = msgs.length - 1; i >= 0; i--) {
                        var msg = msgs[i];
			msg["type"]="foundsome";
                        var json = JSON.stringify(msg);
			connection.sendUTF(json);
                    }
                });

            }
            if (msg.type === "messagelist") {
                var filter = {$or:[{to:{$exists:false}},{to:user.id}]};
                filter["type"] = "message";
                filter["room"] = msg.room;
                if (msg.lang)
                  filter["lang"] = msg.lang;
                if (msg.message_type)
                  filter["message_type"] = msg.message_type;
                else
                  filter["message_type"] = "text";
                if(user.admin|| user.moderator)
                {
                  if(msg.hasOwnProperty("visible"))
                      filter["visible"] = msg.visible;
                }
                else
		{
                    filter["visible"]={$ne:false};
                    filter["baned"]={$ne:true};
		}
		
		connection.cursor=
		db.find(
                    filter
                );
                connection.cursor.sort({ts: -1});
                connection.cursor.skip(20);
                var c = db.find(
                    filter
                );
                c.sort({ts: -1});
                c.limit(20);
                c.toArray(function(err, msgs) {
		    if(err){
			console.error(err);
			return;
                    }
                    for (var i = msgs.length - 1; i >= 0; i--) {
                        var msg = msgs[i];
                        var dbmsg = filterMessage(msg);;
                        var json = JSON.stringify(dbmsg);

			if(msg.to){
				if(msg.to==user.id)
					connection.sendUTF(json);
					//sendToUser(user,json);
				
			}
			else
				connection.sendUTF(json);
				//sendToUser(user,json);

                    }
                });

            }
            if (msg.type === "getunseen") {
                var filter = {};
                filter["type"] = "message";
		filter["to"]=user.id;
		filter["seen"]={$exists:false};
		filter["message_type"]="text";
                var c = db.find(
                    filter
                );
                c.sort({ts: -1});
                c.limit(20);
                c.toArray(function(err, msgs) {
                    for (var i = msgs.length - 1; i >= 0; i--) {
                        var msg = msgs[i];
                        var dbmsg = filterMessage(msg);;
                        var json = JSON.stringify(dbmsg);
			console.log("unseen: "+json);
			//sendToUser(user,json);
			connection.sendUTF(json);

                    }
                });

            }
            if (msg.type === "messagelistfrom") {
                var filter = {};
                filter["type"] = "message";
                filter["room"] = msg.room;
                if (msg.message_type)
                  filter["message_type"] = msg.message_type;
                if (msg.from)
                  filter["from"] = parseInt(msg.from);
                if(user.admin|| user.moderator)
                {
                  if(msg.hasOwnProperty("visible"))
                      filter["visible"] = msg.visible;
                }
                else
                    filter["visible"]={$ne:false};
                var c = db.find(
                    filter
                );
                c.sort({ts: -1});
                c.limit(40);
                c.toArray(function(err, msgs) {
                    for (var i = msgs.length - 1; i >= 0; i--) {
                        var msg = msgs[i];
                        var dbmsg = filterMessage(msg);
                        var json = JSON.stringify(dbmsg);
			if(msg.to)
			{
				var to=parseInt(msg.to);
				userExists(to,function(){
					//sendToUser(user,json);
					connection.sendUTF(json);
				});
			}
			else
				//sendToUser(user,json);
				connection.sendUTF(json);

                    }
                });

            }
            if (msg.type === "messagehistory") {
		var to=parseInt(msg.to);
		var from=parseInt(msg.from);
                var filter = {to:{$exists:true},$or:[{$and:[{to:to},{from:from}]},{$and:[{to:from},{from:to}]}]};
                filter["type"] = "message";
                if (msg.message_type)
                  filter["message_type"] = msg.message_type;
                if(user.admin|| user.moderator)
                {
                  if(msg.hasOwnProperty("visible"))
                      filter["visible"] = msg.visible;
                }
                else
                    filter["visible"]={$ne:false};
                var c = db.find(
                    filter
                );
                c.sort({ts: -1});
                c.limit(40);
                c.toArray(function(err, msgs) {
		    if(err){console.error(err);return;}
                    for (var i = msgs.length - 1; i >= 0; i--) {
                        var msg = msgs[i];
                        var dbmsg = filterMessage(msg);
                        var json = JSON.stringify(dbmsg);
			//sendToUser(user,json);
			connection.sendUTF(json);

                    }
                });

            }
	    if(msg.type==="setname"){
		if(msg.name){
    	        	users.findOneAndUpdate({type: "user",id:user.id}, {$set: {name: msg.name}},{new: true},
		function(err,doc) {
			if(err){
				var json = JSON.stringify({type:"error",err:"nameexists"});
				//sendToUser(user,json);
				connection.sendUTF(json);
			}
			else{
				if(user.name)
					sm(user.name+" => "+msg.name,user.id,connection.room);
				else
					sm(msg.name,user.id,user.room);
					
				user.name=msg.name;
				doc.value["name"]=msg.name;
    				connection.sendUTF(JSON.stringify({type: 'youAreOnline',doc:doc.value}));
				var json = JSON.stringify({type:"setname",name:msg.name});
				//sendToUser(user,json);
				connection.sendUTF(json);
			}
		});
	    	}
	    }
	    if(msg.type==="seen"){
		if(msg.id){
			var id=new mongodb.ObjectID(msg.id);
    	        	db.findOneAndUpdate({_id:id}, {$set: {seen: true}},{new:true},
		function(err,doc) {
			if(err){
				console.error(err);
			}
			else{
				if(doc.to){}
				else{ 

					var seenMsg={type:"seen",id:msg.id};
					sendToRoom(doc.value.room,seenMsg);
				}
				console.log("seen:"+doc);
			}
		});
	    	}
	    }
	    if (msg.type === "userprofile") {
		if(msg.id){
			users.findOne({id: parseInt(msg.id)}, function(e, u) {
        			if (u){
					u["pw"]=undefined;
					u["type"]="userprofile";
					var json = JSON.stringify(u);
					connection.sendUTF(json);
				}
			});
		}
	    }
            if (msg.type === "toggle_message") {
                var id = msg.id;
		var filter={};
		var visible=msg.visible;
		if(!user.moderator&&!user.admin)
			filter["from"]=parseInt(user.id);
		try{
			filter['_id']=new mongodb.ObjectID(id);
		}
		catch(err){
			console.error(err);
		}
		if(msg.lang)
			filter['lang']=msg.lang;
    	        db.update(filter, {$set: {visible: visible,toggled_from:user.id}},{}, function(err,n) 
		{
		if(n==0)return;
                var json = JSON.stringify({
                    type: 'toggle_message',
                    id: id,
		    visible:msg.visible,
                    from:user.id
                });
                for (var cid in clients) {
                    if (clients[cid] == undefined) continue;
                    //console.log(JSON.stringify(client));

		    sendToUser(clients[cid],json);
                }
		});
            }
            if (msg.type === "getrooms") 
	    {
		db.aggregate([{$group:{_id:"$room",count:{$sum:1}}},{$sort:{count:-1}}])
		.toArray(
		function(err, rooms) {
			if(err)return;
			var msg={type:"rooms",rooms:rooms};
			var a=
			JSON.stringify(msg);
			connection.sendUTF(a);
		});
            }
            if (!user.moderator && !user.admin) return;
            if (msg.type === "getlanguages") {

                db.distinct("lang",
			function(err,langs)
			{
				if(err)return;
                    		for (var i = langs.length - 1; i >= 0; i--) {
                        		var lang = langs[i];
                        		var json = JSON.stringify({type:"lang",lang:lang});
					//sendToUser(user,json);
					connection.sendUTF(json);
		    		}
	    		});
	    }
            if (!user.admin) return;
            if (msg.type === "userinfo") {
                var id = parseInt(msg.id);
                // broadcast message to all connected clients
                var type;
                var name = "";
                var admin = false;
                var version = 0;
                var room;
                var ts;
                if (clients[id] === undefined)
                    type = "userisoff";
                else {
                    type = "userison";
                    name = clients[id].name;
                    admin = clients[id].admin;
                    version = clients[id].version;
                    room = clients[id].room;
                    ts = clients[id].ts;
                }
                var cache = [];
		var cons;
		if(clients[id])
                cons=JSON.stringify([...clients[id].con.values()], 
                function(key, value) {
                   if (typeof value === 'object' && value !== null) 
                   {
                      if (cache.indexOf(value) !== -1) 
                      {
                         // Circular reference found, discard key
                         return;
                      }
                      // Store value in our collection
                      cache.push(value);
                   }
                   return value;
               });
               cache = null; // Enable garbage 
                var json = JSON.stringify({
                    type: type,
                    id: id,
                    name: name,
                    admin: admin,
                    room: room,
                    cons: cons,
                    version: version,
                    ts: ts
                });
		//sendToUser(user,json);
		connection.sendUTF(json);
            }
            if (msg.type === "delete") {
                var id = msg.id;
		console.log(id);
		var _id;
		try{
			_id=new mongodb.ObjectID(id);
		}
		catch(e){
			console.error(e);
			return;
		}
                db.deleteOne({
                    _id: _id
                });
                for (var cid in clients) {
                    if (clients[cid] == undefined) continue;
                    //console.log(JSON.stringify(client));

                    var json = JSON.stringify({
                        type: 'delete',
                        id: id
                    });
		    						sendToUser(clients[cid],json);
                }
            }
            if (msg.type === "possible_mods") {
		users.aggregate([{$match:{moderator:{$exists:false},msgratio:{$exists:true,$ne:NaN},name:{$exists:true}}},{$sort:{online_ts:-1}},{$limit: 512},{$sort:{messages:-1}},{$limit:64},{$sort:{created:1}},{$limit:32},{$sort:{msgratio:1}},{$limit:8},{$sort:{messages:-1}}])
		.toArray(
		function(err, users) {
			if(err)return;
			var msg={type:"possible_mods",users:users};
			var a=
			JSON.stringify(msg);
			sm(a,user.id,user.room);
		});
            }
            if (msg.type === "toggle_moderator") {
                var id = parseInt(msg.id);
                var moderator=msg.moderator;
                console.log("moderator:"+id);
    	        users.update({type: "user",id:id}, {$set: {moderator: moderator}},{}, function(err,count) {
console.log(count);
});
		if(clients[id])
			clients[id].moderator=moderator;
            }
            if (msg.type === "ban") {
		if(!msg.id)return;
                var id = parseInt(msg.id);
                var duration=+new Date()+msg.duration;
    	        users.update({id:id}, {$set: {ban: duration}},{}, function(err,count) {
console.log(count);
});
		if(clients[id])
			clients[id].ban=duration;
            }


        }
    }

    function onClose(con) {
        if (user == null) {
            console.log("user null disconnected");
            return;
        }

        //console.log((new Date()) + " Peer " +
          //  connection.remoteAddress + " disconnected.");

        //var index= user.con.indexOf(connection);
        //if (index > -1) {
        //	user.con.splice(index, 1);
        //}
	console.error("onClose:"+user.con.has(con));
	disconnect(user,con);
        // push back user's color to be reused by another user

    }
}
