module.exports.process = process;
String.prototype.capital = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}
String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};
function process(facts,msg,user,callback){
	
	if((typeof user.name)=="string")
	{
		if(!user.bname)
		user.bname=user.name.replace(/ /g,"_")
			.toLowerCase()
	}

	this.facts=facts;
	if((typeof msg)!="string")
		return;
	msg=msg.replace(/<(?:.|\n)*?>/gm, '').replace(/(\r\n|\n|\r)/gm,"");
	msg=msg.toLowerCase()
		.replace(/ +(?= )/g,'');
	msg=msg.replace(/\bdie \b/g,"die_")
		.replace(/\bder \b/g,"der_")
		.replace(/\bdas \b/g,"das_")
		.replace(/\bein \b/g,"ein_")
		.replace(/\beine \b/g,"eine_")
		.replace(/\beinen \b/g,"einen_")
		.replace(/\bdiese \b/g,"diese_")
		.replace(/\bdieser \b/g,"dieser_")
		.replace(/\bdieses \b/g,"dieses_")
		.replaceAll(" des ","_des_")
		.replaceAll("ich bin",user.bname+" ist")
		.replaceAll("bin ich","ist "+user.bname)
		.replaceAll("ich habe",user.bname+" hat")
		.replaceAll("habe ich","hat "+user.bname);
	//var ss=msg.match( /[^\.!\?]+[\.!\?]+/g );
	var ss=msg.split(/\. |\?|\!\: /g);
	if(ss)
        for (var i = 0; i < ss.length; i++)
		processSentence(facts,ss[i].trim(),user,callback);
	else
		processSentence(facts,msg,user,callback);
}
function processSentence(facts,msg,user,callback){
	msg=msg.trim();
	var words=msg.split(" ");
	var v="ist";
	if(words.length<3)return;
	if(!msg.includes("ist "))
	{
		if(msg.includes("hat "))
			v="hat";
		else if(msg.includes("kann "))
			v="kann";
		else if(msg.includes("mag "))
			v="mag";
		else if(msg.includes("macht "))
			v="macht";
		else if(msg.includes("tut "))
			v="tut";
		else if(msg.includes("darf "))
			v="darf";
		else if(msg.includes("will "))
			v="will";
		else if(msg.includes("gibt "))
			v="gibt";
		
		msg=msg.replace(/\bich\b/,user.bname);
	}
	if(msg.startsWith("was "+v+" ")||msg.startsWith("wer ist ")){
		var s=msg.substring(("was "+v+" ").length,msg.length);
		var regex = new RegExp(v+".*");
    		facts.find({s: s,p:regex},
		function(fs){
			var a=s+"<br>";
                    	for (var i = 0; i < fs.length; i++)
			{
				var f=fs[i];
				a+=f.p;
				if(f.count<0)
					a+=" (falsch)";
				a+="<br>"
			}
			callback(a);
		});
		return;
	}
	if(msg.startsWith("warum "+v+" ")||msg.startsWith("wieso "+v+" ")){
		var wq=msg.substring(("warum "+v+" ").length,msg.length);
		var ws=wq.split(" ");
		if(ws.length!=2)
			return;
		ws[1]=v+" "+ws[1];
    		facts.findOne({s: ws[0],p:ws[1]},
			function(f){
				if(!f){
					var a="";
					checkAbility(facts,v,{s: ws[0],p:ws[1]},callback,0,a);

					return;
				}
				if(f.username)
				if(f.count>0){
					var a="das hat "+f.username+ " gesagt"; 
					if(a)
						callback(a);
				}
			});
		return;
	}
	if(msg.startsWith("wann ist ")){
		var wq=msg.substring(("wann ist ").length,msg.length);
		var regex = new RegExp("ist am_.*");
    		facts.findOne({s: wq,p:regex},
			function(f){
				if(f)
				if(f.username)
				if(f.count>0){
					var a=f.s+" "+f.p; 
					if(a)
						callback(a);
				}
			});
		return;
	}
	if(msg.startsWith(v+" ")||msg.startsWith("sind ")){
		var q=msg.substring(msg.indexOf(" ")+1,msg.length);
		var ws=q.split(" ");
		
		if(ws.length!=2)
			return;
		var regex = new RegExp(v+" "+ws[1]);
		var a="Ja,";
		checkAbility(facts,v,{s: ws[0],p:regex},callback,0,a);
	}
	if(msg.includes(" "+v+" ")){
		var inc=1;
		if(msg.includes("nicht")){
			msg=msg.replace("nicht","")
			.replace(/ +(?= )/g,'');
			inc=-1;
		}
		if(msg.match(/kein.? /)){
			msg=msg.replace(/kein.? /,"")
			.replace(/ +(?= )/g,'');
			inc=-1;
		}
		
		var s=msg.substring(0,msg.indexOf(" "+v+" "));
		var p=msg.substring(msg.indexOf(v),msg.length);
		p=p.replace(/am /,"am_");
		p=p.replace(/in /,"in_");
		if(p.split(" ").length!=2)
			return;
		var selector={s:s,p:p};
		var f={s:s,p:p,user:user.id,username:user.bname};
		f["created"]=+new Date();
		facts.update(selector,f,inc);
	}

}
let c=0;
function checkAbility(facts,v,_f,callback,depth,a,found){
	if(depth>10) return;
	depth++;
	facts.findOne(_f,function(fact){
		const f=_f;
		//if(!fact)
		{
		if(depth==1&&!fact)
			a+=" weil";
    		facts.find({s: _f.s,p:/ist ein.?_.*/},
		function(fs){
 			for (var i = 0; i < fs.length; i++)
			{
				var fct=fs[i];
				if(fct.count>0)
				{
					var p=fct.p;
					p=p.split(" ");
					var v=p[0];
					p=p.slice(1,p.length);
					p[p.length]=v;
					p=p.join(" ");
					var s=fct.p.substring(4);
					_f.s=s;
					let f={};
					f.s=s;
					f.p=_f.p;
					var _a=" "+fct.s+" "+p+" und<br>";
					(function(_a){
					checkAbility(facts,v,f,callback,depth,a,function(a){
						if(fact){
							facts.remove(fact);
							console.log("REMOVE FACT");
console.log("removed");
							_a+="weil ";
							//return false;
						}
							console.log(fact);
						if(found)
							_a=found()+_a;
						return _a;

					});
					})(_a);
					
				}
			}
		});
			if(!fact)	
			return;
		}
		if(found){
			a+=found();
		}
		if(fact.count<=0)return;
		var regex = new RegExp("der_bot "+v+"|das_system "+v);
		if(v==="ist")
			v="bin";
		if(v==="hat")
			v="habe";
		if(v==="macht")
			v="mache";
		if(v==="tut")
			v="tue";
		a+=" "+fact.s+" "+fact.p;
		a=a.replace(regex,"ich "+v)
		callback(a.capital());
	});
	return;
}
