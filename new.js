const WebSocket = require('ws');

const wss = new WebSocket.Server({ port: 8080 });

// Broadcast to all.
wss.broadcast = function broadcast(data) {
	wss.clients.forEach(function each(client) {
		if (client.readyState === WebSocket.OPEN) {
			client.send(data);
		}
	});
};
var groups={}

wss.on('connection', function(ws) {
	ws.on('message', function(data) {
		onMessage(ws,data);
	});
});
function onMessage(ws,data){
	wss.sendToAll(ws,data);
}
wss.sendToAll=function(ws,data){
	wss.clients.forEach(function(client) {
		if (client !== ws && client.readyState === WebSocket.OPEN) {
			client.send(data);
		}
	});
}
