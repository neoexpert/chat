"use strict";
process.title = 'node-chat';
const express = require('express');
const compression = require('compression')
const app = express();
const pug = require('pug');
const port = 80;
const fs = require('fs');
const path = require('path');
//const bson = require('bson');
//const BSON = new bson.BSONPure.BSON();
//const Long = bson.BSONPure.Long;
const mongodb = require('mongodb');
const util = require('util');
const wsServer = require('ws').Server;
const https = require('http');
var MongoClient = require('mongodb').MongoClient;
const assert = require('assert');
const bot = require('./bot');
const chatServer = require('./chatServer');
const authlog = require('./authlog');
const GoogleAuth = require('google-auth-library');
var auth = new GoogleAuth;
const GCLIENT_ID="665566840894-f8j7n9dhtgp8hqvimoemhlj429pv3bh5.apps.googleusercontent.com";
var gauthclient = new auth.OAuth2(GCLIENT_ID, '', '');
function glogin(msg,user,con){
	gauthclient.verifyIdToken(
		msg.token,
		GCLIENT_ID,
	function(e, login) {
		if(e){
			console.error(e);
			return;
		}

		var payload = login.getPayload();
		var userid = payload['sub'];
		console.log("google login: "+userid);
		users.findOne({
			type: "user",
			gid: userid
		}, function(e, u) {
		if (u == null)
		{
			users.findOneAndUpdate({type: "user",id:user.id}, {$set: {"gid": userid,guser:payload}},{}, function(err,r) {});
		}
		else{
			if(u.id!=user.id){
				var json2 = JSON.stringify({
					type: 'yourNewID',
					id: u.id,
					pw: u.pw
				});
				console.log(json2);
				con.send(json2,sendcallback);
			}
			users.findOneAndUpdate({type: "user",id:u.id}, {$set: {guser:payload}},{}, function(err,r) {});
		}
		});
		// If request specified a G Suite domain:
		//var domain = payload['hd'];
	});
}
// Connection URL
var url = 'mongodb://neoexpert:1cneo2005@localhost:27017/chat';
var db;
var users;
var nodes;
var factsdb;
var facts={};
function js(j){
	return JSON.stringify(j);
}
facts.update=function(s,fact,inc){
	console.log(fact);
	factsdb.update(s,{$set:fact,$inc:{count:inc}},{upsert:true});
}
function sendcallback(err){
	console.error(err);
}
facts.find=function(f,callback){
				var c=factsdb.find(f);
				c.sort({count:-1});
				c.limit(10);
				c.toArray(
												function(err, msgs) {
												if(err)return;
												if(msgs.length<1)return;
												callback(msgs);
												});
}
/*facts.findOne=function(f,callback){
	var c=factsdb.find(f);
	c.sort({count:-1});
	c.limit(1);
	c.toArray(
	function(err, msgs) {
	if(err)return;
	if(msgs.length>0)
	callback(msgs[0]);
	});
	}*/
facts.remove=function(f,callback){
				factsdb.deleteOne(f);	
}
facts.findOne=function(_f,_callback){
				let f=_f;
				let callback=_callback;
				var c=factsdb.find(f);
				c.sort({count:-1});
				c.limit(1);
				c.toArray(
												function(err, msgs) {
												if(err){
												console.error(err);
												return
												};
												if(msgs.length>0){
												callback(msgs[0]);
												}
												else
												callback();

												});
}

//MongoDB
MongoClient.connect(url, 
function(err, mdb) {
	assert.equal(null, err);
	console.log("MongoDB: CONNECTED");
	db=mdb.collection("chat");
	users=mdb.collection("users");
	users.update({type:"user"}, {$set: {online:false}}, {multi:true}, function() {});
	nodes=mdb.collection("nodes");
	factsdb=mdb.collection("facts");
	startChat(db)
});
var options = {
//key: fs.readFileSync('../cert/privkey.pem'),
//		 cert: fs.readFileSync('../cert/cert.pem'),
//		 ca: [fs.readFileSync( '../cert/AddTrustExternalCARoot.crt'),fs.readFileSync( '../cert/COMODORSAAddTrustCA.crt'),fs.readFileSync('../cert/COMODORSADomainValidationSecureServerCA.crt')]
};
options = {
  //key: fs.readFileSync('/etc/letsencrypt/live/neochat.tk/privkey.pem'),
  //cert: fs.readFileSync('/etc/letsencrypt/live/neochat.tk/cert.pem'),
  //ca: fs.readFileSync('/etc/letsencrypt/live/neochat.tk/chain.pem')
};
app.set('view engine', 'pug');
function wwwRedirect(req, res, next) {
    if (req.headers.host.slice(0, 4) === 'www.') {
        var newHost = req.headers.host.slice(4);
        return res.redirect(301, req.protocol + '://' + newHost + req.originalUrl);
    }
    next();
};

app.set('trust proxy', true);
app.use(wwwRedirect);
app.use(compression({filter: shouldCompress}))

function shouldCompress (req, res) {
  if (req.headers['x-no-compression']) {
    // don't compress responses with this request header
    return false
  }

  // fallback to standard filter function
  return compression.filter(req, res)
}
//app.use(express.static('./htmls'));

/*app.get('/sitemap.xml', function (req, res) {
	res.writeHead(200, { 'Content-Type': "text/xml" });
	db.aggregate([{$group:{_id:"$room",count:{$sum:1}}},{$sort:{count:-1}}])
	.toArray(function(err, rooms) {
		if(err)return;
		res.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
		res.write("<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\n");
	var asize = rooms.length;
	for (var i = 0; i < asize; i++) {
		res.write("\t<url>\n");
		res.write("\t\t<loc>https://neochat.tk/"+rooms[i]._id+"</loc>\n");
		res.write("\t</url>\n");
	}	
		res.write("</urlset>");
		res.end();
	});

});
*/
/*
app.get('/*', function(req, res) {
	var node=req.path.substring(1);
	nodes.findOne({
		path: req.path
	},function(err, r) {
			if(!r){
  			res.render('chatroom', { title:node , desc:"Chat Room"});
				return;
			}
			if(req.query.edit)
			{
				console.dir(r);
  			res.render('editnode', r);
				return;
			}
			switch(r.type){
				case "room":
					if((typeof r.template) == "string")
  					res.render(r.template, { node:node,title:r.title , desc: r.desc,js:!!(r.js)});
					break;
				case "node":
					res.set('Content-Type', r.ctype);
					var html=pug.render(r.value, r)
					res.end(html);
				break;
				default:
					res.set('Content-Type', r.type);
					res.end(r.value);
				}
		});
	console.log("GET "+req.originalUrl);
	console.log("User-Agent: "+req.get("User-Agent"));
});
*/
var server;
// = https.createServer(options,app);
function startChat(mdb){
	server = https.createServer(app);
	server.listen(port, function() {
		console.log((new Date()) + " Server is listening on port " + port);
		if(process.getgid()=="root"){
		process.setgid('adm');
		process.setuid('alex');
		}
		chatServer.start(this,db,users,factsdb,nodes);
		process.on('uncaughtException', function (err) 
		{
			chatServer.sendToAdmin(err+"");
  		console.error(err);
		});
	});
}
