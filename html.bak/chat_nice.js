window.onerror = Fehlerbehandlung;
 
    function Fehlerbehandlung (Nachricht, Datei, Zeile) {
      Fehler = "Fehlermeldung:\n" + Nachricht + "\n" + Datei + "\n" + Zeile;
      zeigeFehler();
      return true;
    }
 
    function zeigeFehler () {
      alert(Fehler);
    }

function onClose() {
$("#chat").html("disconnected");
}
function onOpen(){}
function js(j){
return JSON.stringify(j);
}
var lang = navigator.language || navigator.userLanguage; 
function sendMsg(ele) {
    if(event.keyCode == 13) {
	var m={type:"usermsg",message:ele.value,message_type:"text"}
	if(lang)
		m["lang"]=lang;
	if(country)
		m["country"]=country;
  	c.send(js(m));

    }
}
function private_chat(id){
}
function refer(id){
}
function processMsg(msg){
switch(msg.message_type){
	case "text":
	var m="";
	m+="<div id='"+msg._id+"' class='msg'>";
	m+="<div class='name'>"
	if(msg.name)
		m+=msg.name;
	else
		m+="user";
	if(msg.country){
		m+=" "+flag(msg.country.toUpperCase());
	}
	m+="</div>";
	m+="<br>";
	m+="<div class='msgtext'>"
	m+=msg.message.replace(/\<(?!br|font|span|\/span).*?\>/g, "");
	m+="</div>";

	if(msg.seen)
		m+="<div class='seen'>✓<div>";
	m+="</div>";
	var me=$(m);
	me.on("click",function(){
	$("#menu").remove();
	var menu=""
	menu+="<div id='menu'>"
	menu+="<button onclick='private_chat("+msg.from+")'>chat<button>"
	menu+="<button onclick='refer("+msg.from+")'>refer<button>"
	menu+="</div>";
	me.append(menu);
	});
	$("#chat").append(me);
	$("#chat").finish();;
	$("#chat").animate({ scrollTop: $('#chat').prop("scrollHeight")}, 1000);
	if(msg.from==myID||msg.refer==myID)
	{
		me.css("background-color","#222222");
	}
	if(msg.color)
	{
		me.find(".name").css("color",toColor(msg.color));
	}
	break;
	case "rocket":
		launch();
	break;
}
}
function toColor(num) {
    num >>>= 0;
    var b = num & 0xFF,
        g = (num & 0xFF00) >>> 8,
        r = (num & 0xFF0000) >>> 16,
        a = ( (num & 0xFF000000) >>> 24 ) / 255 ;
    return "rgba(" + [r, g, b, a].join(",") + ")";
}
var c;
var ls;
$(function(){
	ls=localStorage;
	msginput=$("#msginput");
	c= new WebSocket("ws://" + location.host);
	c.onopen = onOpen;
	c.onclose = onClose;
	c.onerror = onError;
	c.onmessage = onMessage;
	var canvas = document.getElementById('canvas');
       var context = canvas.getContext('2d');

    // resize the canvas to fill browser window dynamically
    window.addEventListener('resize', resizeCanvas, false);

    function resizeCanvas() {
            canvas.width = window.innerWidth;
            canvas.height = window.innerHeight;

    }
    resizeCanvas();
var pressTimer;
(function($) {
    $.fn.longClick = function(callback, timeout) {
        var timer;
        timeout = timeout || 500;
        $(this).mousedown(function() {
            timer = setTimeout(function() { callback(); }, timeout);
            return false;
        });
        $(document).mouseup(function() {
            clearTimeout(timer);
            return false;
        });
    };

})(jQuery);
//$(window).longClick(function(){
//alert();
//});
});
function onError(error) {
$("body").append(error);
  console.log('WebSocket Error ' + error);
};

var myID;
var room;
var users=0;
var msginput;

function onMessage(e) {
	var msg=JSON.parse(e.data);	
	//$("body").append(e.data+"<br>");
	switch(msg.type){
		case "yourID":
		myID=localStorage.getItem("myID");
		var pw=localStorage.getItem("pw");
		var l={type:"myID",myID:myID,pw:pw,version:"web"};
		var mycolor=ls.getItem("color");
		room=window.location.pathname.substring(1);
		if(room=="chat.html")
			room="scdown";
		if(room=="")
			room="default";
		if(room)
		l["room"]=room;
		if(mycolor)
			l["color"]=mycolor;
		else{
			l["color"]=color;
			ls.setItem("color", color);
		}
  		c.send(js(l));
		break;
		case "yourNewID":
		ls.setItem("myID", msg.id);
		ls.setItem("pw", msg.pw);
		break;
		case "youAreOnline":
  		c.send(js({type:"messagelist",room:room}));
		break;
		case "usermsg":
			processMsg(msg);
		break;
		case "seen":
		$("#"+msg.id).find("msgtext").append("<div class='seen'>✓<div>");
		break;
		case "received":
		msginput.val("");
		break;
		case "userison":
			users++;	
		msginput.attr("placeholder", "online: "+users);
		break;
		case "userisoff":
			users--;
		break;
		case "toggle_message":
		case "delete":
		$("#"+msg.id).hide();
		break;
		default:
		alert(e.data);
		break;
	}
};
