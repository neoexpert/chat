//window.onerror = Fehlerbehandlung;
 
    function Fehlerbehandlung (Nachricht, Datei, Zeile) {
      Fehler = "Fehlermeldung:\n" + Nachricht + "\n" + Datei + "\n" + Zeile;
      zeigeFehler();
      return true;
    }
 
    function zeigeFehler () {
      alert(Fehler);
    }
document.addEventListener('DOMContentLoaded', function () {
  if (!Notification) {
    return;
  }

  if (Notification.permission !== "granted")
    Notification.requestPermission();
});
//navigator.serviceWorker.register('sw.js');

function showNotification() {
  Notification.requestPermission(function(result) {
    if (result === 'granted') {
      navigator.serviceWorker.ready.then(function(registration) {
        registration.showNotification('Vibration Sample', {
          body: 'Buzz! Buzz!',
          icon: '../images/touch/chrome-touch-icon-192x192.png',
          vibrate: [200, 100, 200, 100, 200, 100, 200],
          tag: 'vibration-sample'
        });
      });
    }
  });
}
function imNotARobot(){
  	var captcha_response = grecaptcha.getResponse();
  	c.send(js({type:"recaptcha",key:captcha_response}));
}
function onClose() {
	cl.loggedIn=false;
	connect();
	view.onClose();	
}
function onOpen(){
 	window.onfocus = function() {connect(); };
	view.onOpen();
}
function js(j){
return JSON.stringify(j);
}
var lang = navigator.language || navigator.userLanguage; 
if(lang)
lang=lang.substring(0,2);
function sendMsg(event,me) {
    if(event.keyCode == 13) {
	var msg=me.value;
	msg=msg.replace(/ +(?= )/g,'');
	if(msg==="")return;
	if(me.value.startsWith("/nick "))
	{
		var name=msg.substring(msg.indexOf(" ")+1,msg.length);
		var m={type:"setname",name:name}
  		c.send(js(m));
		return;
	}
	var m={type:"usermsg",message:msg,message_type:"text"}
	if(lang)
		m["lang"]=lang;
	if(country)
		m["country"]=country;
	if(ref){
		m["refer"]=ref;
		ref=undefined;
	}
  	c.send(js(m));

    }
}
function private_chat(id){
}
function hide(_id){
	var hm={type:"toggle_message",id:_id,visible:false,lang:lang};
	c.send(js(hm));
}
function delete_message(_id){
	var hm={type:"delete",id:_id};
	c.send(js(hm));
}
var ref;
function refer(id,name){
	ref=id;
var txtarea = document.getElementById("msginput");
    // obtain the index of the first selected character
    var start = txtarea.selectionStart;
    // obtain the index of the last selected character
    var finish = txtarea.selectionEnd;
    //obtain all Text
    var allText = txtarea.value;

    // obtain the selected text
    var sel = allText.substring(start, finish);
    //append te text;
    var newText=allText.substring(0, start)+"@"+name+allText.substring(finish, allText.length);

    console.log(newText);

    txtarea.value=newText;
}
function processMsg(msg,next){
switch(msg.message_type){
	case "text":
		if(!msg.baned)
		view.addMessage(msg,next);
	break;
	case "rocket":
		launch();
	break;
}
}
var cl={};
cl.send=function(o){
  c.send(js(o));
}
cl.glogin=function(token){
	cl.gtoken=token;
	if(cl.loggedin)
		c.send(js({type:"glogin",token:token}));
}
var ls;
cl.nextMessage=function(){
	c.send(js({type:"next"}));
}
function connect(){
	if(c)
		if(c.readyState==1)return;
	c= new WebSocket("wss://" + location.host);
	c.onopen = onOpen;
	c.onclose = onClose;
	c.onerror = onError;
	c.onmessage = onMessage;
}
var c;
var room="scdown";
cl.moderator=false;
$(function(){
 room=window.location.pathname.substring(1);
	ls=localStorage;
	msginput=$("#msginput");
	connect();
});
function onError(error) {
$("body").append(error);
  console.log('WebSocket Error ' + error);
};

var users=0;
var msginput;

function onMessage(e) {
	var msg=JSON.parse(e.data);	
	//$("body").append(e.data+"<br>");
	switch(msg.type){
		case "error":
			alert(msg.err);
		break;
		case "yourID":
		cl.myID=localStorage.getItem("myID");
		var pw=localStorage.getItem("pw");
		var l={type:"myID",myID:cl.myID,pw:pw,version:"web"};
		var mycolor=ls.getItem("color");
		if(room=="chat.html")
			room="scdown";
		if(room=="")
			room="default";
		if(room)
		l["room"]=room;
		if(!mycolor)
		{
			l["color"]=color;
			ls.setItem("color", color);
		}
  		c.send(js(l));
		break;
		case "YourNewID":
		ls.setItem("myID", msg.id);
		ls.setItem("pw", msg.pw);
		break;
		case "youAreOnline":
			cl.loggedIn=true;
  		c.send(js({type:"messagelist",room:room}));
			if(cl.gtoken)
				c.send(js({type:"glogin",token:cl.gtoken}));
			cl.gtoken=undefined;
		var me=msg.doc;
		if(me)
		if(!me.gid&&!me.not_a_bot)
		{
			$(".g-recaptcha").show();
			$("#msginput").hide();
		}
		break;
		case "not_a_bot":
			$(".g-recaptcha").hide();
			$("#msginput").show();
		break;
		case "youaremoderator":
		cl.moderator=true;
		break;
		case "youareadmin":
		cl.moderator=true;
		cl.admin=true;
		break;
		case "usermsg":
			processMsg(msg);
		break;
		case "nextmsg":
			processMsg(msg,true);
		break;
		case "seen":
		$("#"+msg.id).find("msgtext").append("<div class='seen'>✓<div>");
		break;
		case "received":
		msginput.val("");
		break;
		case "userison":
			if(msg.room==room)
			users++;	
		msginput.attr("placeholder", "online: "+users);
		break;
		case "userisoff":
			if(msg.room==room)
			users--;
		break;
		case "toggle_message":
		case "delete":
		$("#"+msg.id).hide();
		break;
		default:
		alert(e.data);
		break;
	}
};
