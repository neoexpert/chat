var view={};
$(function(){
$('#header__icon').click(function(e){
      e.preventDefault();
      $('body').toggleClass('with--sidebar');
    });

    $('#site-cache').click(function(e){
      $('body').removeClass('with--sidebar');
    });

$("#chat").scroll(function() {
	if($(this).scrollTop() < 500)
		cl.nextMessage();
	view.lastScroll=+new Date();
});
});
view.lastScroll=0;
var audio = new Audio('/media/ding.mp3');
view.onOpen=function(){
	$("#chat").html("");
};
view.onClose=function(){
	$("#chat").html("disconnected");
};
view.addMessage=function(msg,next){
	var m="";
	m+="<div id='"+msg._id+"' class='msg'>";
	m+="<span class='name'>"
	if(msg.name)
		m+=msg.name;
	else
		m+="user";
	if(msg.country){
		m+=" "+flag(msg.country.toUpperCase());
	}
	m+="</span>";
	m+="<span class='info'>"
	if(!msg.visible&&msg.toggled_from){
		m+="hidden from ";
		m+=msg.toggled_from;
		m+=" ";
	}
	if(msg.moderator)
		m+="m";
	if(msg.admin)
		m+="a";
	m+="</span>";
		//audio.play();
	if (document.hidden) {
		if(!msg.history)
		audio.play();
		//alert(msg.message);
		//showNotification();
	}
	m+="<br>";
	m+="<div class='msgtext'>"
	if(msg.to===cl.myID)
	{
		m+=msg.message.replace(/\<(?!br|font|span|\/span).*?\>/g, "")+" (private)";
	}
	else
		m+=msg.message.replace(/\<(?!br|font|span|\/span).*?\>/g, "");
	m+="</div>";

	if(msg.seen)
		m+="<span class='seen'>✓<span>";
	m+="</div>";
	var me=$(m);
	me.on("click",function(){
	$("#menu").remove();
	var menu=""
	menu+="<div id='menu'><br>"
	menu+="<button onclick='private_chat("+msg.from+")'>chat</button>"
	menu+="<button onclick=\"refer("+msg.from+",'"+msg.name+"')\">refer</button>";
	if(msg.from==cl.myID||cl.moderator)
		menu+="<button onclick=\"hide('"+msg._id+"')\">hide</button>";
	if(cl.admin)
		menu+="<button onclick=\"delete_message('"+msg._id+"')\">delete</button>";
	menu+="</div>";
	me.append(menu);
	});
	if(next){
		var st=$("#chat").scrollTop();
		$("#chat").prepend(me);
		$("#chat").scrollTop(st+me.outerHeight());
	}
	else
		$("#chat").append(me);
	$("#chat").finish();
	if(!next&&view.lastScroll<+new Date()+10000)
		$("#chat").animate({ scrollTop: $('#chat').prop("scrollHeight")}, 1000);
	if(msg.from==cl.myID||msg.refer==cl.myID)
	{
		me.css("background-color","#222222");
	}
	if(msg.color)
	{
		me.find(".name").css("color",toColor(msg.color));
	}
}
function toColor(num) {
    num >>>= 0;
    var b = num & 0xFF,
        g = (num & 0xFF00) >>> 8,
        r = (num & 0xFF0000) >>> 16,
        a = ( (num & 0xFF000000) >>> 24 ) / 255 ;
    return "rgba(" + [r, g, b, a].join(",") + ")";
}
